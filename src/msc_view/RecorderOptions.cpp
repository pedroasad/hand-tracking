#include <msc_view/RecorderOptions.h>

#include <boost/program_options.hpp>
#include <msc/OptionsParser.h>

namespace po = boost::program_options;

namespace lcg {
namespace msc {

void RecorderOptions::registerOptions() {
	OptionsParser::instance().configOptions.add_options()
		("Recorder.active",
			po::value<bool>(&active)->default_value(false),
			"[bool] Switches tracker output recording to 'output/' directory On/Off\n\r"
		);
}

void RecorderOptions::validateOptions() {
}

} /* namespace lcg::msc */
} /* namespace lcg */
