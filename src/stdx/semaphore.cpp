#include <stdx/semaphore.h>

namespace stdx {

semaphore::semaphore (int count) :
		count(count) {
}

void
semaphore::signal() {
	std::unique_lock<std::mutex> lock(mtx);
	count++;
	cv.notify_one();
}

void
semaphore::wait() {
	std::unique_lock<std::mutex> lock(mtx);

	while(count == 0){
		cv.wait(lock);
	}
	count--;
}

} /* namespace stdx */
