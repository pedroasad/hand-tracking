#include <libfreenect_extra.h>
#include <stdlib.h>
#include <string.h>

char*
freenect_resolution_name(freenect_resolution resolution, char *resolution_name) {

	const char *message;
	size_t message_length;

	switch (resolution) {
		case FREENECT_RESOLUTION_LOW:
			message = "QVGA (320x240)";
			break;

		case FREENECT_RESOLUTION_MEDIUM:
			message = "VGA (640x480)";
			break;

		case FREENECT_RESOLUTION_HIGH:
			message = "SXGA (1280x1024)";
			break;

		default:
			message = "unknown";
			break;
	}

	message_length = strlen(message);

	if (resolution_name == NULL)
		resolution_name = (char*) malloc((message_length + 1) * sizeof(char));

	strncpy(resolution_name, message, message_length);
	resolution_name[message_length] = '\0';

	return resolution_name;
}

char*
freenect_depth_mode_name(freenect_frame_mode depth_mode, char *depth_mode_name) {

	const char *message, *separator = " ";
	char *resolution_name;
	size_t message_length, resolution_name_length, total_length, separator_length;

	switch (depth_mode.depth_format) {
		case FREENECT_DEPTH_11BIT:
			message = "11 bit depth information in one uint16_t/pixel";
			break;

		case FREENECT_DEPTH_10BIT:
			message = "10 bit depth information in one uint16_t/pixel";
			break;

		case FREENECT_DEPTH_11BIT_PACKED:
			message = "11 bit packed depth information";
			break;

		case FREENECT_DEPTH_10BIT_PACKED:
			message = "10 bit packed depth information";
			break;

		case FREENECT_DEPTH_REGISTERED:
			message = "processed depth data in mm, aligned to 640x480 RGB";
			break;

		case FREENECT_DEPTH_MM:
			message = "depth to each pixel in mm, but left unaligned to RGB image";
			break;

		default:
			message = "unknown";
			break;
	}

	resolution_name = freenect_resolution_name(depth_mode.resolution, NULL);

	message_length         = strlen(message);
	resolution_name_length = strlen(resolution_name);
	separator_length       = strlen(separator);
	total_length = message_length + resolution_name_length + separator_length;

	if (depth_mode_name == NULL)
		depth_mode_name = (char*) malloc((total_length + 1) * sizeof(char));

	strncpy(depth_mode_name                                            , resolution_name, resolution_name_length);
	strncpy(depth_mode_name + resolution_name_length                   , separator      ,       separator_length);
	strncpy(depth_mode_name + resolution_name_length + separator_length, message        ,         message_length);

	depth_mode_name[total_length] = '\0';

	return depth_mode_name;
}

char*
freenect_video_mode_name(freenect_frame_mode video_mode, char *video_mode_name) {

	const char *message, *separator = " ";
	char *resolution_name;
	size_t message_length, resolution_name_length, total_length, separator_length;

	switch (video_mode.depth_format) {
	case FREENECT_VIDEO_RGB:
			message = "Decompressed RGB mode (demosaicing done by libfreenect)";
			break;

		case FREENECT_VIDEO_BAYER:
			message = "Bayer compressed mode (raw information from camera)";
			break;

		case FREENECT_VIDEO_IR_8BIT:
			message = "8-bit IR mode";
			break;

		case FREENECT_VIDEO_IR_10BIT:
			message = "10-bit IR mode";
			break;

		case FREENECT_VIDEO_IR_10BIT_PACKED:
			message = "10-bit packed IR mode";
			break;

		case FREENECT_VIDEO_YUV_RGB:
			message = "YUV RGB mode";
			break;

		case FREENECT_VIDEO_YUV_RAW:
			message = "YUV Raw mode";
			break;

		case FREENECT_VIDEO_DUMMY:
		default:
			message = "unknown";
			break;
	}

	resolution_name = freenect_resolution_name(video_mode.resolution, NULL);

	message_length         = strlen(message);
	resolution_name_length = strlen(resolution_name);
	separator_length       = strlen(separator);
	total_length = message_length + resolution_name_length + separator_length;

	if (video_mode_name == NULL)
		video_mode_name = (char*) malloc((total_length + 1) * sizeof(char));

	strncpy(video_mode_name                                            , resolution_name, resolution_name_length);
	strncpy(video_mode_name + resolution_name_length                   , separator      ,       separator_length);
	strncpy(video_mode_name + resolution_name_length + separator_length, message        ,         message_length);

	video_mode_name[total_length] = '\0';

	return video_mode_name;
}
