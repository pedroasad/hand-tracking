#include <kinect_viewer/shared_buffer.h>

namespace lcg {
namespace msc {

shared_buffer::shared_buffer(size_t slot_size, int slots) :
		slot_size(slot_size),
		slots(slots),
		buffer(::operator new(slot_size * slots)),
		pos_read(0),
		pos_write(0),
		sem_read(0),
		sem_write(slots) {
}

shared_buffer::~shared_buffer() {
	::operator delete(buffer);
}

int shared_buffer::consume(std::function<void(void*)> consume_function) {
	int current_slot;

	sem_read.wait();

	current_slot = std::atomic_fetch_add(&pos_read, 1);
	consume_function(buffer + (current_slot % slots) * slot_size);

	sem_write.signal();

	return current_slot;
}

int shared_buffer::produce(std::function<void(void*)> produce_function) {
	int current_slot;

	sem_write.wait();

	current_slot = std::atomic_fetch_add(&pos_write, 1);
	produce_function(buffer + (current_slot % slots) * slot_size);

	sem_read.signal();

	return current_slot;
}

void* shared_buffer::read_address() {
	return buffer + pos_read % slots;
}

const void* shared_buffer::read_address() const {
	return buffer + pos_read % slots;
}

void* shared_buffer::write_address() {
	return buffer + pos_write % slots;
}

const void* shared_buffer::write_address() const {
	return buffer + pos_write % slots;
}

} /* namespace msc */
} /* namespace lcg */
