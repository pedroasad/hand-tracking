#include <kinect_viewer/CameraDisplay.h>

#include <cuimg/functions/apply.h>
#include <kinect_viewer/CameraDisplayOptions.h>
#include <msc/EnvironmentOptions.h>
#include <msc/KinectSpecs.h>
#include <msc/PSOOptions.h>
#include <msc/System.h>
#include <msc/camera/CameraOptions.h>
#include <msc/camera/PlaybackCamera.h>
#include <msc_view/Visualizer.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <stdx/string.h>
#include <tucano/framebuffer.hpp>

const std::string
	display_window_name = "Hand-tracking",
	min_depth_tb        = "Min. depth",
	max_depth_tb        = "Max. depth";

namespace lcg {
namespace msc {

const cv::Size kinect_size(KinectSpecs::width, KinectSpecs::height);

CameraDisplay::CameraDisplay() {
	cv::namedWindow(display_window_name, cv::WINDOW_AUTOSIZE);
	cv::resizeWindow(display_window_name, KinectSpecs:: width, KinectSpecs::height);
	cv::namedWindow(display_window_name);
	cv::createTrackbar(min_depth_tb, display_window_name, &min_depth, KinectSpecs::possibleDepthRange());
	cv::createTrackbar(max_depth_tb, display_window_name, &max_depth, KinectSpecs::possibleDepthRange());
}

void CameraDisplay::processEvents() {
	switch(waitKey()) {
	case 'g':
		break;

	case 'n':
		if (CameraOptions::instance().cameraMode == CameraModeEnum::PLAYBACK)
			dynamic_cast<PlaybackCamera*>(system().camera)->nextFrame();
		break;

	case 'p':
		if (CameraOptions::instance().cameraMode == CameraModeEnum::PLAYBACK)
			dynamic_cast<PlaybackCamera*>(system().camera)->pause();
		break;

	case 'P':
		if (CameraOptions::instance().cameraMode == CameraModeEnum::PLAYBACK)
			dynamic_cast<PlaybackCamera*>(system().camera)->play();
		break;

	case 27:
	case 'q':
	case 'Q':
		system().shutdown();
		break;
	}

	glfwPollEvents();
}

void CameraDisplay::run() {
	cv::imshow(display_window_name, Visualizer::instance().montageImageOutput);
	processEvents();
}


int CameraDisplay::waitKey() {
	//TODO: Hard-coded delay
	if (CameraOptions::instance().cameraMode == CameraModeEnum::PLAYBACK) {
		if (dynamic_cast<PlaybackCamera*>(system().camera)->isPaused())
			return cv::waitKey() & 255;
		else
			return cv::waitKey(1000 / 60.0) & 255;
	} else
		return cv::waitKey(1000 / 60.0) & 255;
}

} /* namespace msc */
} /* namespace lcg */
