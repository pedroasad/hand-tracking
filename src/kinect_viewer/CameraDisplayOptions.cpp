#include <kinect_viewer/CameraDisplayOptions.h>

#include <boost/program_options.hpp>
#include <msc/OptionsParser.h>

namespace po = boost::program_options;

namespace lcg {
namespace msc {

void CameraDisplayOptions::registerOptions() {
	OptionsParser::instance().configOptions.add_options();
}

void CameraDisplayOptions::validateOptions() {
}

} /* namespace lcg::msc */
} /* namespace lcg */
