#include <msc/OptionsParser.h>
#include <msc/System.h>
#include <msc_view/Visualizer.h>

using namespace lcg;
using namespace msc;

int main(int argc, char *argv[]) {
//	OptionsParser::instance().configFiles << "eval.ini";
	system().init(argc, argv);
	system().addModule(&Visualizer::instance());
	system().run();

	exit(EXIT_SUCCESS);
}
