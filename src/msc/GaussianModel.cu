#include <msc/GaussianModel.h>

namespace lcg {
namespace msc {

__host__ __device__
GaussianModel::GaussianModel(const GaussianModel &model) :
		mean(model.mean), covariance(model.covariance) {
}

__host__ __device__
GaussianModel::GaussianModel(const vec2f &mean, const mat2f &covariance) :
		mean(mean), covariance(covariance) {
}

__host__ __device__
float GaussianModel::density(const vec2f &point) const {
	float exponent = (point - mean).transposed() * covariance.inverse() * (point - mean);
	float denom = CUDART_SQRT_2PI * sqrt(/*2 * CUDART_PI_F **/ covariance.determinant());
	return exp(-exponent / 2) / denom;
}

} /* namespace msc */
} /* namespace lcg */
