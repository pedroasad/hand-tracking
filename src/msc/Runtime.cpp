/*
 * Runtime.cpp
 *
 *  Created on: 13/02/2016
 *      Author: pedro
 */

#include <GL/gl.h>
#include <msc/KinectSpecs.h>
#include <msc/Runtime.h>

namespace lcg {
namespace msc {

Runtime::Runtime() {
}

int Runtime::maxParticlesPerRender() {
	int maxFramebufferHeight;
	glGetIntegerv(GL_MAX_FRAMEBUFFER_HEIGHT, &maxFramebufferHeight);
	return maxFramebufferHeight / KinectSpecs::height;
}

} /* namespace msc */
} /* namespace lcg */
