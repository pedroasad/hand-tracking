#include <msc/camera/CameraFactory.h>

#include <msc/camera/CameraOptions.h>
#include <msc/camera/KinectCamera.h>
#include <msc/camera/PlaybackCamera.h>

namespace lcg {
namespace msc {

CameraFactory::CameraFactory() :
		theguy(nullptr) {
}

AbstractCamera* CameraFactory::make() {
	if (theguy == nullptr) {
		//TODO: Change this for automatic registration in AbstractCamera subtypes (I guess this would be an Abstract Factory Pattern, right?)
		switch (CameraOptions::instance().cameraMode) {
		case CameraModeEnum::KINECT:
			theguy = new KinectCamera;
			break;

		case CameraModeEnum::PLAYBACK:
			theguy = new PlaybackCamera;
			break;
		}
	}

	return theguy;
}

} /* namespace msc */
} /* namespace lcg */
