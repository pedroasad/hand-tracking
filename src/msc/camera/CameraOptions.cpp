#include <msc/camera/CameraOptions.h>

#include <boost/program_options.hpp>
#include <msc/OptionsParser.h>

namespace lcg {
namespace msc {

namespace po = boost::program_options;

void CameraOptions::registerOptions() {
	OptionsParser::instance().configOptions.add_options()
		("Camera.cameraMode",
			po::value<std::string>(&cameraModeString)->default_value("kinect"),
			"[string] Camera type used for input (playback|kinect)\n\r");
}

void CameraOptions::validateOptions() {
	if (cameraModeString == "kinect")
		cameraMode = CameraModeEnum::KINECT;
	else if (cameraModeString == "playback")
		cameraMode = CameraModeEnum::PLAYBACK;
	else {
		std::cerr << "Fatal error: Unrecognized Camera.cameraMode value: " << cameraModeString << std::endl;
		exit(EXIT_FAILURE);
	}
}

} /* namespace msc */
} /* namespace lcg */
