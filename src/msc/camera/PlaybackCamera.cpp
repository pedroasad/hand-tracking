#include <msc/camera/PlaybackCamera.h>

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>
#include <msc/types.h>
#include <msc/Clock.h>
#include <msc/EnvironmentOptions.h>
#include <msc/KinectSpecs.h>
#include <msc/System.h>
#include <msc/camera/PlaybackCameraOptions.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace lcg {
namespace msc {

namespace fs = boost::filesystem;
using boost::regex;
using boost::regex_match;

const cv::Size kinect_size(KinectSpecs::width, KinectSpecs::height);

PlaybackCamera::PlaybackCamera() :
		paused(PlaybackCameraOptions::instance().startPaused) {

	regex depthRegex(PlaybackCameraOptions::instance().depthWildcard);
	regex videoRegex(PlaybackCameraOptions::instance().videoWildcard);

	fs::directory_iterator depthTraversal(PlaybackCameraOptions::instance().depthDirectory);
	while (depthTraversal != fs::directory_iterator()) {
		if (regex_match(depthTraversal->path().filename().string(), depthRegex))
			depthFiles << depthTraversal->path().string();
		depthTraversal++;
	}

	fs::directory_iterator videoTraversal(PlaybackCameraOptions::instance().videoDirectory);
	while (videoTraversal != fs::directory_iterator()) {
		if (regex_match(videoTraversal->path().filename().string(), videoRegex))
			videoFiles << videoTraversal->path().string();
		videoTraversal++;
	}

	if (depthFiles.size() == 0) {
		std::cerr << "No depth images found for pathspec \"" <<
			PlaybackCameraOptions::instance().depthDirectory << "/"  <<
			PlaybackCameraOptions::instance().depthWildcard  << "\"" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (videoFiles.size() == 0) {
		std::cerr << "No video images found for pathspec \"" <<
			PlaybackCameraOptions::instance().videoDirectory << "/"  <<
			PlaybackCameraOptions::instance().videoWildcard  << "\"" << std::endl;
		exit(EXIT_FAILURE);
	}

	std::sort(depthFiles.begin(), depthFiles.end());
	std::sort(videoFiles.begin(), videoFiles.end());

	currentDepthFile = depthFiles.begin();
	currentVideoFile = videoFiles.begin();
}

bool PlaybackCamera::isPaused() {
	return paused;
}

void PlaybackCamera::nextFrame() {
	currentDepthFile++;
	currentVideoFile++;
}

void PlaybackCamera::pause() {
	paused = true;
	currentDepthFile--;
	currentVideoFile--;
}

void PlaybackCamera::play() {
	paused = false;
}

void PlaybackCamera::run() {
	if (currentDepthFile == depthFiles.end() or currentVideoFile == videoFiles.end()) {
		if (PlaybackCameraOptions::instance().loop) {
			currentDepthFile = depthFiles.begin();
			currentVideoFile = videoFiles.begin();
		} else {
			system().shutdown();
		}
	}

	cv::Mat depth = cv::imread(*currentDepthFile, CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);
	cv::Mat video = cv::imread(*currentVideoFile);

	cv::cvtColor(video, video, cv::COLOR_BGR2RGB);

	//TODO: Check RGB - BGR consistency
	memcpy(observedDepthOutput->data, depth.data, sizeof(ObservedDepthImage));
	memcpy(observedVideoOutput->data, video.data, sizeof(ObservedVideoImage));

	Clock::instance().logTime("\nacq_out:");

	if (not paused)
		nextFrame();
}

} /* namespace msc */
} /* namespace lcg */
