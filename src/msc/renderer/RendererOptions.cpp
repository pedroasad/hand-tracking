#include <msc/renderer/RendererOptions.h>

#include <boost/program_options.hpp>
#include <msc/OptionsParser.h>

namespace po = boost::program_options;

namespace lcg {
namespace msc {

void RendererOptions::registerOptions() {
	//TODO: This option is just a temporary workaround because camera parameters seem to be slightly imprecise
	OptionsParser::instance().configOptions.add_options()
		("Renderer.depthCorrection",
			po::value<float>(&depthCorrection)->default_value(-0.05),
			"[float] Percent of total depth range correction applied to rendered poses\n\r");
}

void RendererOptions::validateOptions() {
}

} /* namespace msc */
} /* namespace lcg */
