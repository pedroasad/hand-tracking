#include <cutils/thread_index.h>

#include <msc/constants.h>
#include <msc/CudaOptions.h>
#include <msc/optimizer/ClassicOptimization.h>
#include <msc/EnvironmentOptions.h>
#include <msc/KinectSpecs.h>
#include <msc/optimizer/ClassicOptimization.h>
#include <msc/PSOOptions.h>
#include <msc/PSODebuggerOptions.h>
#include <msc/Runtime.h>
#include <msc/System.h>
#include <msc/optimizer/ObjectiveFunction.h>
#include <thrust/reduce.h>
#include <thrust/sort.h>
#include <thrust/iterator/counting_iterator.h>

#include <cmath>

namespace lcg {
namespace msc {

using cutils::thread_index;
using stdx::vector;
using cutils::vec2i;

template <class ObjectiveFunction>
__global__
void computeEnergyElements(
		ObservedDepthImage &observedDepthInput ,
		RenderedDepthImage *renderedDepthInputs,
		ObservedSkinImage  &observedSkinInput  ,
		FunctionOutputs buffers,
		ObjectiveFunction function,
		bool writeRenderMatches) {

	vec2i p = thread_index().xy();
	int n = thread_index().z();

	FunctionInputs inputs{
		observedDepthInput[p],
		255 - observedSkinInput[p], // TODO: Legacy code still works with inverted background
		renderedDepthInputs[n][p]
	};

	function(n, p, inputs, buffers, writeRenderMatches);
}

ClassicOptimization::ClassicOptimization() :
		depthDifferenceOutputsPointer(thrust::raw_pointer_cast(depthDifferenceOutputs.data())),
		skinAndMatchesOutputsPointer (thrust::raw_pointer_cast(skinAndMatchesOutputs .data())),
		skinOrMatchesOutputsPointer  (thrust::raw_pointer_cast(skinOrMatchesOutputs  .data())),
		//TODO: Hard-coded domain constants
		abductionDistribution(-M_PI_4, M_PI_4),
		flexionDistribution(-M_PI_2, M_PI_2),
		influenceDistribution(0, 1),
		//TODO: Produce a random engine to pick values from an object pool
		fingerJointDistribution(
			int(HandDofs::THUMB_CARPOMETACARPAL_JOINT_ABDUCTION),
			HAND_DOFS - 1
		),
		randomAbduction  (std::bind(  abductionDistribution, randomGenerator)),
		randomFlexion    (std::bind(    flexionDistribution, randomGenerator)),
		randomInfluence  (std::bind(  influenceDistribution, randomGenerator)),
		randomFingerJoint(std::bind(fingerJointDistribution, randomGenerator)) {
}

void ClassicOptimization::advanceSwarm() {
	for (int i = 0; i < PSOOptions::instance().particles; i++) {
		HandPose &x = particlesOutputs[i];
		HandPose &p = bestCasesOutputs[i];
		HandPose &g = particlesOutputs[indexByRankOutputs[0]];

		float w  = constrictionFactor();
		float c1 = PSOOptions::instance().cognitiveComponent;
		float c2 = PSOOptions::instance().socialComponent;

		velocitiesOutputs[i] +=
			c1 * randomInfluence() * (p - x) +
			c2 * randomInfluence() * (g - x);

		velocitiesOutputs[i] *= w;
	}

	//TODO: The operation below might be implemented as a single matrix sum, although the performance gain is probably not tremendous.
	for (int i = 0; i < PSOOptions::instance().particles; i++) {
		particlesOutputs[i] += velocitiesOutputs[i];
		//TODO: Oikonomidis does not apply a simple clamping here, but a nearest point projection
		particlesOutputs[i].clamp(
			PSOOptions::instance().lowerPoseBound(),
			PSOOptions::instance().upperPoseBound()
		);
	}

	Clock::instance().logTime(";opt_update:");
}

void ClassicOptimization::disturbSwarm() {
	for (int i = 0; i < PSOOptions::instance().disturbedParticles(); i++) {
		int j = randomFingerJoint();
		if (HandPose::isAnAbductionAngle(HandDofs(j)))
			particlesOutputs[i](j) = randomAbduction();
		else
			particlesOutputs[i](j) = randomFlexion();

		//TODO: There is no need to clamp the whole particle, but at the same time, iterating through dimensions is so poor, since we have the PSO::poseAmplitude method
		particlesOutputs[i].clamp(
			PSOOptions::instance().lowerPoseBound(),
			PSOOptions::instance().upperPoseBound()
		);
	}

	Clock::instance().logTime(";opt_disturb:");
}

void ClassicOptimization::evaluateSwarm() {
	using cutils::thread_index;
	using stdx::vector;

	//TODO: To improve energy calculation by prefix sum, perform as few prefix sums as possible, then apply a kernel to copy each image's results into a linear vector with as many slots as particles and apply an out-place exclusive sum to this vector. Finally, subtract this vector from the previous one to obtain each image's energy sum. Perhaps this could be applied in cpu to avoid setup time.

	// Render current set of hypotheses to framebuffer
	// TODO: Best way of achieving this is to publish particle hypothesis and have the renderer subscribe to this subject.
	system().renderer->renderPosesToFbo(
		particlesOutputs.begin(), particlesOutputs.end()
	);

	Clock::instance().logTime(";opt_render:");

	dim3 threadBlock = CudaOptions::instance().defaultBlock();
	dim3 threadGrid  = CudaOptions::instance().defaultGrid();
	threadGrid.z *= PSOOptions::instance().particles;

	FunctionOutputs buffers{
		thrust::raw_pointer_cast(depthDifferenceOutputs.data()),
		thrust::raw_pointer_cast(  renderMatchesOutputs.data()),
		thrust::raw_pointer_cast( skinAndMatchesOutputs.data()),
		thrust::raw_pointer_cast(  skinOrMatchesOutputs.data())
	};

	//TODO: Adopted switch based solution because cuda virtual method approach did not work at first attempt. Since no objects containing virtual methods may be passed to a kernel, a device factory must be configured to return abstract ObjectiveFunction references -- it might be even possible to automatically populate a __device__ singleton for this purpose but additional reading and tests are required.
	switch (PSOOptions::instance().objectiveFunction) {
	case ObjectiveFunctionEnum::CLASSIC:
		computeEnergyElements<<<threadGrid, threadBlock>>>(
			system().preprocessor->processedDepthOutput,
			thrust::raw_pointer_cast(System::instance().renderer->renderedDepthOutputs.data()),
			system().skinDetector->observedSkinOutput,
			buffers,
			OikonomidisFunction(),
			PSODebuggerOptions::instance().active
		);
		break;

	case ObjectiveFunctionEnum::MODIFIED:
		computeEnergyElements<<<threadGrid, threadBlock>>>(
			system().preprocessor->processedDepthOutput,
			thrust::raw_pointer_cast(System::instance().renderer->renderedDepthOutputs.data()),
			system().skinDetector->observedSkinOutput,
			buffers,
			ModifiedFunction(),
			PSODebuggerOptions::instance().active
		);
		break;
	}

	CUDA_DEBUG( cudaDeviceSynchronize() );
	Clock::instance().logTime(";opt_energy:");

	for (int n = 0; n < PSOOptions::instance().particles; n++) {

		EnergyType depthDifferenceSum = thrust::reduce(
			thrust::device_ptr<EnergyType>(&depthDifferenceOutputsPointer[n    ].data[0]),
			thrust::device_ptr<EnergyType>(&depthDifferenceOutputsPointer[n + 1].data[0])
		);

		EnergyType skinAndMatchesSum = thrust::reduce(
			thrust::device_ptr<EnergyType>(&skinAndMatchesOutputsPointer[n    ].data[0]),
			thrust::device_ptr<EnergyType>(&skinAndMatchesOutputsPointer[n + 1].data[0])
		);

		EnergyType skinOrMatchesSum = thrust::reduce(
			thrust::device_ptr<EnergyType>(&skinOrMatchesOutputsPointer[n    ].data[0]),
			thrust::device_ptr<EnergyType>(&skinOrMatchesOutputsPointer[n + 1].data[0])
		);

		CUDA_DEBUG( cudaDeviceSynchronize() );

		float energy1 = float(depthDifferenceSum) / (float(skinOrMatchesSum) + PSOOptions::instance().energyEpsilon);
		float energy2 = (1 - 2.0f * skinAndMatchesSum / (skinOrMatchesSum + skinAndMatchesSum));
		float energy3 =
			std::max({0.0f,
				particlesOutputs[n](int(HandDofs::MIDDLE_METACARPOPHALANGEAL_JOINT_ABDUCTION)) -
				particlesOutputs[n](int(HandDofs:: INDEX_METACARPOPHALANGEAL_JOINT_ABDUCTION))
			}) +
			std::max({0.0f,
				particlesOutputs[n](int(HandDofs::  RING_METACARPOPHALANGEAL_JOINT_ABDUCTION)) -
				particlesOutputs[n](int(HandDofs::MIDDLE_METACARPOPHALANGEAL_JOINT_ABDUCTION))
			}) +
			std::max({0.0f,
				particlesOutputs[n](int(HandDofs:: PINKY_METACARPOPHALANGEAL_JOINT_ABDUCTION)) -
				particlesOutputs[n](int(HandDofs::  RING_METACARPOPHALANGEAL_JOINT_ABDUCTION))
			})
		;

		//TODO: Implement remaining energies
		float score =
			PSOOptions::instance().energyComponent1 * energy1 +
			PSOOptions::instance().energyComponent2 * energy2 +
			PSOOptions::instance().energyComponent3 * energy3;

		if (score < scoresOutputs[n])
			bestCasesOutputs[n] = particlesOutputs[n];

		if (score < bestScoreEverOutput) {
			bestScoreEverOutput = score;
			bestCaseEverOutput = particlesOutputs[n];
		}

		scoresOutputs[n] = score;
	}

	thrust::host_vector<float> scoresCopy = scoresOutputs;

	thrust::counting_iterator<int> indices(0);
	thrust::copy(
		indices, indices + indexByRankOutputs.size(),
		indexByRankOutputs.begin()
	);

	thrust::sort_by_key(
		scoresCopy.begin(), scoresCopy.end(),
		indexByRankOutputs.begin()
	);

	CUDA_DEBUG( cudaDeviceSynchronize() );
	Clock::instance().logTime(";opt_rank:");
}

} /* namespace lcg::msc */
} /* namespace lcg */
