#include <msc/optimizer/ObjectiveFunction.h>

#include <cutils/thread_index.h>
#include <msc/EnvironmentOptions.h>

namespace lcg {
namespace msc {

__constant__ OikonomidisFunctionParams oikonomidisParams;
__constant__    ModifiedFunctionParams    modifiedParams;

using cutils::thread_index;
using cutils::vec2i;

void ObjectiveFunction::initialize() {
	OikonomidisFunctionParams oikonomidisParamsHost{
		400, 400,
		1000 * EnvironmentOptions::instance().nearDepthClip,
		1000 * EnvironmentOptions::instance(). farDepthClip
	};

	ModifiedFunctionParams modifiedParamsHost{
		400, 400, 500,
		1000 * EnvironmentOptions::instance().nearDepthClip,
		1000 * EnvironmentOptions::instance(). farDepthClip
	};

	CUDA_DEBUG( cudaMemcpyToSymbol(oikonomidisParams, &oikonomidisParamsHost, sizeof(OikonomidisFunctionParams)) );
	CUDA_DEBUG( cudaMemcpyToSymbol(   modifiedParams, &   modifiedParamsHost, sizeof(   ModifiedFunctionParams)) );
}

__device__
ObjectiveFunction::~ObjectiveFunction() {
}

__device__
void OikonomidisFunction::operator()(
		int n, vec2i p,
		FunctionInputs in,
		FunctionOutputs out,
		bool writeRenderMatches) {

	EnergyType depthDiff = abs(in.od - in.rd);

	EnergyType rm = (depthDiff <= oikonomidisParams.depthThreshold2) or (in.od == 0); // TODO: Funny thing that they included this in rm because there are plenty of holes in the depth map, which translates into direct contamination. Hence, I'm excluding this condition, as well as filtering all depth values that are beyond a depth clipping limit (which should be set according to kernel parameters relating to the preprocessing phase that is currently disabled). Well, they actually imply that all depth values too distant from the palm center should be zeroed, but 0 is just what od is compared to! I guess the correct solution is segmenting the image into: skin (strong and weak), non-skin and background

	out.depthDifferenceOutputs[n][p] = min(depthDiff, oikonomidisParams.depthThreshold1); //TODO: The problem with the original formulation is that it is too much affected by the background
	//TODO: These 2 guys could be output to a single buffer if using a wide integer type and segmenting the bits
	out.skinAndMatchOutputs   [n][p] = (in.os and rm);
	out.skinOrMatchOutputs    [n][p] = (in.os or  rm);
	if (writeRenderMatches)
		out.depthMatchesOutputs[n][p] = rm;
}

__device__
void ModifiedFunction::operator()(
		int n, vec2i p,
		FunctionInputs in,
		FunctionOutputs out,
		bool writeRenderMatches) {

	EnergyType depthDiff = abs(in.od - in.rd);

	bool observedDepthInRange = (modifiedParams.nearDepthClip <= in.od) and (in.od <= modifiedParams.farDepthClip);
	bool renderedDepthInRange = (modifiedParams.nearDepthClip <= in.rd) and (in.rd <= modifiedParams.farDepthClip);
	bool allDepthsInRange     = (observedDepthInRange) and (renderedDepthInRange);
	bool acceptableDepthDiff  = allDepthsInRange or (observedDepthInRange and in.os) or renderedDepthInRange;

	EnergyType rm =
		(allDepthsInRange and (depthDiff <= modifiedParams.depthThreshold2)) or
		(renderedDepthInRange and (in.od == 0)); // TODO: Funny thing that they included this in rm because there are plenty of holes in the depth map, which translates into direct contamination. Hence, I'm excluding this condition, as well as filtering all depth values that are beyond a depth clipping limit (which should be set according to kernel parameters relating to the preprocessing phase that is currently disabled)

	out.depthDifferenceOutputs[n][p] = acceptableDepthDiff ? min(depthDiff, modifiedParams.depthThreshold1) : 0; //TODO: The problem with the original formulation is that it is too much affected by the background
	//TODO: These 2 guys could be output to a single buffer if using a wide integer type and segmenting the bits
	out.skinAndMatchOutputs   [n][p] = (in.os and rm);
	out.skinOrMatchOutputs    [n][p] = (in.os or  rm);

	if (writeRenderMatches)
		out.depthMatchesOutputs[n][p] = rm;
}

}
}
