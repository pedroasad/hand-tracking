#include <msc/HandPose.h>

namespace lcg {
namespace msc {

const stdx::vector<int>
HandPose::start = {
	 0, -1, -1,
	 7, -1,  9, -1, 10, -1, -1,
	11, -1, 13, -1, 14, -1, -1,
	15, -1, 17, -1, 18, -1, -1,
	19, -1, 21, -1, 22, -1, -1,
	23, -1, 25, -1, 26, -1, -1
};

const stdx::vector<int>
HandPose::dofs = {
	7, 0, 0,
	//TODO: Review 5th column, which corresponds to DIP joint
	2, 0, 1, 0, 1, 0, 0,
	2, 0, 1, 0, 1, 0, 0,
	2, 0, 1, 0, 1, 0, 0,
	2, 0, 1, 0, 1, 0, 0,
	2, 0, 1, 0, 1, 0, 0,
};

} /* namespace msc */
} /* namespace lcg */
