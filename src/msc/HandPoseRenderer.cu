#include <msc/HandPoseRenderer.h>

using namespace lcg::msc;
using namespace Eigen;

namespace lcg {
namespace msc {

HandPoseRenderer::HandPoseRenderer() :
		hand_model(lcg::msc::HandModel::LEFT),
		fbo(
			KinectSpecs::width,
			KinectSpecs::height * min(
				PSOOptions::instance().particles,
				Runtime::instance().maxParticlesPerRender()
			),
		1),
		swarm(PSOOptions::instance().particles),
		renderedDepthOutputs(PSOOptions::instance().particles),
		renderedDepthBuffers(PSOOptions::instance().particles) {

	camera.setPerspectiveMatrix(
		KinectSpecs::instance().fovy,
		KinectSpecs::instance().aspectRatio,
		KinectSpecs::instance().near,
		KinectSpecs::instance().far
	);

	light.setPerspectiveMatrix(
		KinectSpecs::instance().fovy,
		KinectSpecs::instance().aspectRatio,
		KinectSpecs::instance().near,
		KinectSpecs::instance().far
	);
}

void HandPoseRenderer::renderPosesToFbo(std::initializer_list<HandPose> poses) {
	renderPosesToFbo(poses.begin(), poses.end());
}

void HandPoseRenderer::renderPosesToWindow(std::initializer_list<HandPose> poses) {
	renderPosesToWindow(poses.begin(), poses.end());
}

void HandPoseRenderer::renderStuff(std::initializer_list<HandPose> poses, int rows, int cols, bool invertY) {
	renderStuff(poses.begin(), poses.end(), rows, cols, invertY);
}

} /* namespace msc */
} /* namespace lcg */
