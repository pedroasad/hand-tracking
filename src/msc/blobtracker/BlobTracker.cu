#include <msc/blobtracker/BlobTracker.h>

namespace lcg {
namespace msc {

BlobTracker::BlobTracker() :
		trackingStrategy(BlobTracking::make()) {
}

void BlobTracker::run() {
	trackingStrategy->track();
}

} /* namespace lcg::msc */
} /* namespace lcg */
