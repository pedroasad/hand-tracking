#include <msc/blobtracker/SimpleBlobTracking.h>

#include <cutils/thread_index.h>
#include <cutils/types.h>
#include <msc/System.h>
#include <thrust/scan.h>
#include <thrust/device_vector.h>

namespace lcg {
namespace msc {

template <int N>
__global__
void copy_relevant_blobs(
		cuccl::full_label_image &labels,
		cuccl::properties &properties,
		cuccl::full_label_image &relabels,
		some_properties<N> &hypotheses,
		cuccl::full_label num_blobs) {

	const int i = cutils::thread_index().plain();

	const cuccl::full_label label = labels.data[i];
	const cuccl::full_label relabel = relabels.data[i];

	if (label.value() == i and relabel < N and relabel < num_blobs) {
		hypotheses.labels   (relabel.value()) = label;
		hypotheses.areas    (relabel.value()) = properties.area    (i);
		hypotheses.centroids(relabel.value()) = properties.centroid(i);
		hypotheses.variances(relabel.value()) = properties.variance(i);
	}
}

__global__
void filter_small_and_weird_blobs(cuccl::segment_image &observed_skin_host_image, cuccl::full_label_image &labels, cuccl::properties &props, int min_size) {
	int i = cutils::thread_index().plain();

	cuccl::full_label            label = labels(i);
	cuccl::full_label::base_type root  = label.value();

	if (label.is_background())
		return;

	if (props.area(root) < min_size or label.segment() == segment_low) {
		labels   (i) = cuccl::   full_label::background();
		observed_skin_host_image(i) = cuccl::segment_image::background();
	}
}

__global__
void prepare_relabel(
		const cuccl::full_label_image &labels,
		cuccl::full_label_image &blob_indices) {

	const cutils::vec2i p = cutils::thread_index().xy();
	const int i = cutils::thread_index().plain();

	if (labels(i).value() == i)
		blob_indices(i) = 1;
	else
		blob_indices(i) = 0;
}

void SimpleBlobTracking::filter_blobs() {
	CUDA_CHECK( cudaDeviceSynchronize() );

	copy_relevant_blobs<<<kinect_config::grid, kinect_config::block>>>(*cucclalg.labels, *cucclalg.props, *relabels, *relevant_blobs_dev, cuccl::full_label(num_blobs->value()));
	relevant_blobs = relevant_blobs_dev;

	//TODO: Find out the best blobs (highest area and most extreme centroids?)
	//TODO: Invert the rendered scene y-coordinate
}

void SimpleBlobTracking::perform_ccl() {
	cucclalg.cucclprops(system().skinDetector->observedSkinOutput);

	filter_small_and_weird_blobs<<<kinect_config::grid, kinect_config::block>>>(
		system().skinDetector->observedSkinOutput, cucclalg.labels, cucclalg.props, min_blob_size
	);

	prepare_relabel<<<kinect_config::grid, kinect_config::block>>>(
		cucclalg.labels, relabels
	);

	thrust::device_ptr<cuccl::full_label_image::element_type> relabels_ptr(relabels->data);
	thrust::exclusive_scan(relabels_ptr, relabels_ptr + KinectSpecs:: width * KinectSpecs::height, relabels_ptr);
	num_blobs = cutils::device_ref<cuccl::full_label_image::element_type>(relabels->data + KinectSpecs:: width * KinectSpecs::height - 1);
}

void SimpleBlobTracking::track() {
	perform_ccl();
	filter_blobs();

	CUDA_DEBUG( cudaDeviceSynchronize() );
	Clock::instance().logTime(";blob:");

	//TODO: If using a standard labeling algorithm instead of TCLE, there's no need to copy it over to a similar buffer; instead the blobsOutput could use a thrust::device_ptr instead of a device_vector.
	system().blobTracker->blobLabelsOutput = cucclalg.labels;

	CUDA_DEBUG( cudaDeviceSynchronize() );
	Clock::instance().logTime(";blob_out:");
}

} /* namespace lcg::msc */
} /* namespace lcg */
