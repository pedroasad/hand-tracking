#include <msc/blobtracker/BlobTracking.h>

#include <msc/blobtracker/SimpleBlobTracking.h>

namespace lcg {
namespace msc {

BlobTracking* BlobTracking::make() {
	return new SimpleBlobTracking;
}

BlobTracking::~BlobTracking() {
}

} /* namespace lcg::msc */
} /* namespace lcg */
