#include <msc/PSOOptions.h>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <msc/OptionsParser.h>

#include <cmath>
#include <limits>

namespace fs = boost::filesystem;
namespace po = boost::program_options;

namespace lcg {
namespace msc {

int PSOOptions::disturbedParticles() {
	return particles * disturbingRatio;
}

HandPose PSOOptions::lowerPoseBound() {
	float inf = std::numeric_limits<float>::infinity();
	float r = M_PI / 180;
	HandPose bound;
	bound <<
		-1, -1, -1, -1,
		-inf, -inf, -inf,
		-50 * r, -60 * r,  -45 * r, -90 * r,
		-15 * r, -90 * r, -110 * r, -90 * r,
		-15 * r, -90 * r, -110 * r, -90 * r,
		-15 * r, -90 * r, -110 * r, -90 * r,
		-15 * r, -90 * r, -110 * r, -90 * r;
	return bound;
}

HandPose PSOOptions::upperPoseBound() {
	float inf = std::numeric_limits<float>::infinity();
	float r = M_PI / 180;
	HandPose bound;
	bound <<
		1, 1, 1, 1,
		inf, inf, inf,
		15 * r, 30 * r, 0 * r, 45 * r,
		15 * r,  0 * r, 0 * r,  0 * r,
		15 * r,  0 * r, 0 * r,  0 * r,
		15 * r,  0 * r, 0 * r,  0 * r,
		15 * r,  0 * r, 0 * r,  0 * r;
	return bound;
}

HandPose PSOOptions::poseAmplitude() {
	return upperPoseBound() - lowerPoseBound();
}

void PSOOptions::registerOptions() {
	OptionsParser::instance().configOptions.add_options()
		("PSO.active",
			po::value<bool>(&active)->default_value(true),
			"[bool] Switch PSO module On/Off\n\r"
		)
		("PSO.abductionAmplitude",
			po::value<float>(&abductionAmplitude)->default_value(0.15),
			"[float] Amplitude of abduction angle perturbation (% of joint amplitude)\n\r")
		("PSO.disturbanceVariance",
			po::value<float>(&disturbanceVariance)->default_value(0.3),
			"[float] Variance of total joint amplitude that may be applied to each joint during initialization of a new swarm from a previous completed optimization (% of joint amplitudes)\n\r")
		("PSO.flexionAmplitude",
			po::value<float>(&flexionAmplitude)->default_value(0.15),
			"[float] Amplitude of flexion angle perturbation (% of joint amplitude)\n\r")
		("PSO.quaternionAmplitude",
			po::value<float>(&quaternionAmplitude)->default_value(0.5),
			"[float] Amplitude of hand orientation perturbation (quaternion components)\n\r")
		("PSO.xyAmplitude",
			po::value<float>(&xyAmplitude)->default_value(0.03),
			"[float] Amplitude of hand position perturbation, parallel to image plane (in meters)\n\r")
		("PSO.zAmplitude",
			po::value<float>(&zAmplitude)->default_value(0.015),
			"[float] Amplitude of hand position perturbation, perpendicular to image plane (in meters)\n\r")
		("PSO.particles",
			po::value<int>(&particles)->default_value(64),
			"[int] Number of PSO particles\n\r"
		)
		("PSO.maxGenerations",
			po::value<int>(&maxGenerations)->default_value(25),
			"[int] Maximum number of swarm iterations for each frame\n\r"
		)
		("PSO.disturbingInterval",
			po::value<int>(&disturbingInterval)->default_value(3),
			"[int] Number of intervals between swarm disturbing\n\r"
		)
		("PSO.disturbingRatio",
			po::value<float>(&disturbingRatio)->default_value(0.5f),
			"[float] Ratio of swarm particles that get disturbed every few iterations\n\r"
		)
		("PSO.objectiveFunction",
			po::value<std::string>(&objectiveFunctionName)->default_value("modified"),
			"[string] objective function used in particle evaluation (classic|modified)\n\r"
		)
		("PSO.initialPosesDir",
			po::value<std::string>(&initialPosesDir)->default_value(""),
			"[string] Directory containing *.pose files to initialize poses\n\r"
		)
		("PSO.energyEpsilon",
			po::value<float>(&energyEpsilon)->default_value(0.001f),
			"[float] Energy calculation bias to avoid division by zero\n\r"
		)
		("PSO.cognitiveComponent",
			po::value<float>(&cognitiveComponent)->default_value(2.8f),
			"[float] Cognitive component of particle update phase; must sum no more less 4 with PSO.socialComponent\n\r"
		)
		("PSO.socialComponent",
			po::value<float>(&socialComponent)->default_value(1.3f),
			"[float] Social component of particle update phase; must sum no more less 4 with PSO.cognitiveComponent\n\r"
		)
		("PSO.energyComponent1",
			po::value<float>(&energyComponent1)->default_value(1.0f),
			"[float] Importance of 1st component of energy calculation (average depth difference term)\n\r"
		)
		("PSO.energyComponent2",
			po::value<float>(&energyComponent2)->default_value(20.0f),
			"[float] Importance of 2nd component of energy calculation (quality of skin segmentation and depth matches term)\n\r"
		)
		("PSO.energyComponent3",
			po::value<float>(&energyComponent3)->default_value(10.0f),
			"[float] Importance of 3rd component of energy calculation (kinematic plausibility term)\n\r"
		)
	;
}

void PSOOptions::validateOptions() {
	if (particles <= 0) {
		std::cerr << "Fatal configuration error: PSO.particles cannot be <= 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (disturbingInterval <= 0) {
		std::cerr << "Fatal configuration error: PSO.disturbingInterval cannot be <= 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (disturbingRatio < 0 or disturbingRatio > 1) {
		std::cerr << "Fatal configuration error: PSO.disturbingRatio is bound to the [0..1] range" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (maxGenerations <= 0) {
		std::cerr << "Fatal configuration error: PSO.maxGenerations cannot be <= 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (not fs::exists(initialPosesDir)) {
		std::cerr << "Fatal configuration error: PSO.initialPosesDir \"" << initialPosesDir << "\": no such file or directory" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (not fs::is_directory(initialPosesDir)) {
		std::cerr << "Fatal configuration error: PSO.initialPosesDir \"" << initialPosesDir << "\: is not a directory" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (energyEpsilon <= 0) {
		std::cerr << "Fatal configuration error: PSO.energyEpsilon cannot be <= 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (cognitiveComponent + socialComponent < 4) {
		std::cerr << "Fatal configuration error: PSO.cognitiveComponent + PSO.socialComponent must be >= 4" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (objectiveFunctionName == "classic")
		objectiveFunction = ObjectiveFunctionEnum::CLASSIC;
	else if (objectiveFunctionName == "modified")
		objectiveFunction = ObjectiveFunctionEnum::MODIFIED;
	else {
		std::cerr << "Fatal error: Unrecognized PSO.objectiveFunction value: " << objectiveFunctionName << std::endl;
		exit(EXIT_FAILURE);
	}
}

} /* namespace msc */
} /* namespace lcg */
