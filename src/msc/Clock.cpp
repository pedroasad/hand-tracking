#include <msc/Clock.h>

namespace lcg {
namespace msc {

Clock::Clock() :
		initialized(false),
		timeFile("time.log") {
}

Clock::TimeUnit Clock::check() {
	if (initialized) {
		return std::chrono::duration_cast<TimeUnit>(
			RealClock::now() - epoch
		);
	} else {
		initialized = true;
		epoch = RealClock::now();
		return std::chrono::duration_cast<TimeUnit>(
			epoch - epoch
		);
	}
}

void Clock::logTime(const stdx::string &message) {
	timeFile << message;
	timeFile << check().count();
	timeFile.flush();
}

} /* namespace msc */
} /* namespace lcg */
