#include <msc/KinectSpecs.h>

namespace lcg {
namespace msc {

KinectSpecs::KinectSpecs() :
	aspectRatio(float(width) / height),
	fovy(45.0),
	near( 0.4),
	far ( 4.0) {
}

} /* namespace msc */
} /* namespace lcg */
