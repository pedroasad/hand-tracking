#include <msc/CudaOptions.h>

#include <msc/KinectSpecs.h>

namespace lcg {
namespace msc {

dim3 CudaOptions::defaultBlock() const {
	return dim3(16, 16);
};

dim3 CudaOptions::defaultGrid() const {
	return dim3(
		KinectSpecs::width  / defaultBlock().x,
		KinectSpecs::height / defaultBlock().y
	);
}

} /* namespace msc */
} /* namespace lcg */
