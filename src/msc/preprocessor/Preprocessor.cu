#include <msc/preprocessor/Preprocessor.h>

#include <msc/Clock.h>
#include <msc/EnvironmentOptions.h>
#include <msc/System.h>
#include <msc/preprocessor/PreprocessorOptions.h>
#include <opencv2/imgproc/imgproc.hpp>

namespace lcg {
namespace msc {

const cv::Size kinect_size(KinectSpecs::width, KinectSpecs::height);

Preprocessor::Preprocessor() {
}

void Preprocessor::run() {

	observedDepthInput = system().camera->observedDepthOutput;
	observedVideoInput = system().camera->observedVideoOutput;

	Clock::instance().logTime(";prep_in:");

	//TODO: Perform all preprocessing in GPU
//	CUDA_DEBUG( cudaDeviceSynchronize() );

	cv::Mat video_mat_bgr(kinect_size, CV_8UC3, observedVideoInput->data);

	//TODO: All of the following is not acquisition, but image processing stuff
	cv::Mat depthTooNear(ObservedDepthImage::cvSize, CV_8UC1);
	cv::Mat depthTooFar (ObservedDepthImage::cvSize, CV_8UC1);
	cv::Mat depthFloat  (ObservedDepthImage::cvSize, CV_32FC1);

	cv::Mat(*observedDepthInput).convertTo(depthFloat, CV_32F, 0.001);

	cv::threshold(depthFloat, depthTooNear, EnvironmentOptions::instance().nearDepthClip, 255, cv::THRESH_BINARY_INV);
	cv::threshold(depthFloat, depthTooFar , EnvironmentOptions::instance(). farDepthClip, 255, cv::THRESH_BINARY);

	// TODO: This is pretty stupid, but after thresholding, the matrices are changed into an invalid type (no longer CV_8UC1, but an odd type identifier that corresponds to no actual type). Perhaps file a bug?
	depthTooNear.convertTo(depthTooNear, CV_8U);
	depthTooFar .convertTo(depthTooFar , CV_8U);

	cv::Mat(*observedDepthInput).setTo(0, depthTooNear | depthTooFar);

	//TODO:
	cv::medianBlur(video_mat_bgr, video_mat_bgr, 2 * PreprocessorOptions::instance().medianBlurRadius + 1);

	Clock::instance().logTime(";prep:");

	processedDepthOutput = observedDepthInput;
	processedVideoOutput = observedVideoInput;

	CUDA_DEBUG( cudaDeviceSynchronize() );
	Clock::instance().logTime(";prep_out:");

	//	cuimg::threshold<<<kinect_config::grid, kinect_config::block>>>(
	//		*observedDepthDeviceOutput, uint16_t(min_possible_depth + min_depth), uint16_t(min_possible_depth + max_depth), uint16_t(0), uint16_t(0)
	//	);
}

} /* namespace msc */
} /* namespace lcg */
