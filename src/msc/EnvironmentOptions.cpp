#include <msc/EnvironmentOptions.h>

#include <boost/program_options.hpp>
#include <msc/OptionsParser.h>
#include <msc/KinectSpecs.h>

namespace po = boost::program_options;

namespace lcg {
namespace msc {

void EnvironmentOptions::registerOptions() {
	OptionsParser::instance().configOptions.add_options()
		("Environment.nearDepthClip",
			po::value<float>(&nearDepthClip)->default_value(0.4),
			"[float] Near depth clipping (in meters)\n\r")

		("Environment.farDepthClip" ,
			po::value<float>(& farDepthClip)->default_value(0.9),
			"[float]  Far depth clipping (in meters)\n\r" )
	;
}

void EnvironmentOptions::validateOptions() {
	if (nearDepthClip < 0) {
		std::cerr << "Fatal configuration error: Environment.nearDepthClip cannot be < 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (farDepthClip < 0) {
		std::cerr << "Fatal configuration error: Environment.farDepthClip cannot be < 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	if (nearDepthClip > farDepthClip) {
		std::cerr << "Fatal configuration error: Environment.nearDepthClip cannot be greater than Environment.farDepthClip" << std::endl;
		exit(EXIT_FAILURE);
	}
}

} /* namespace msc */
} /* namespace lcg */
