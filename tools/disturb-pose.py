#!/usr/bin/python
"""Generate random variations of a given hand pose.

The input is given as a file where each line contains a single element of the
pose vector. Each of the n (defaults to 64) generated matrices is output to a
single file with the very same format.
"""
from math import log
import numpy as np
from numpy import array, savetxt
from numpy.random import random
from sys import argv, exit
from textwrap import dedent

if len(argv) < 3:
	print dedent('''\
	Usage:
		%s <pose-file> <output-prefix> [number]
	''' % __file__)
	exit(1)

inputfile  = argv[1]
outputpref = argv[2]

if '.' in inputfile:
	outputsuf  = '.' + inputfile.split('.')[-1]
else:
	outputsuf = ''

if len(argv) > 3:
	samples = int(argv[3])
else:
	samples = 64

amplitude = array([
	0.5, 0.5, 0.5, 0.5,
	0.10, 0.10, 0.10,
	0.50, 1.50, 1.50, 1.50,
	0.50, 1.50, 1.50, 1.50,
	0.50, 1.50, 1.50, 1.50,
	0.50, 1.50, 1.50, 1.50,
	0.50, 1.50, 1.50, 1.50,
])

basepose = array(open(inputfile).readlines()).astype(np.float)

for i in range(samples):
	outputfile = ('%s-%%0%dd%s' % (outputpref, log(samples, 10) + 1, outputsuf)) % (i+1)
	newpose = basepose + amplitude * (random(len(basepose)) - 0.5)
	savetxt(outputfile, newpose)
