#Project roadmap

|             |             |             |
| ----------- | ----------- | ----------- |
| __Start date:__       | 2015-08-17 | |
| __Desired end date:__ | 2016-03-31 | |
| __Number of weeks:__  | 31         | |

## 1<sup>st</sup> week (08/17 to 08/21): Color-based skin detection with a Gaussian model in C++

## 2<sup>nd</sup> week (08/24 to 08/28): Skin-detection on the GPU with hysteresis

## 3<sup>rd</sup> week (08/31 to 09/04): CCL and hypothesis tracking

__Week goal:__ realtime tracking of skin-colored blobs using hypothesis formulation
__Pre-requisites__:
	* CCL with properties

Day   | Estimated hours | Effective hours | Goal
----- | --------------- | --------------- | ----
mon	| 3   | | Planning and CCL
tue	| 6   | | Sequential relabeling
wen	| 6   | | Hypothesis formulation
thu	| 6   | | Hypothesis validation
fri | 4.5 | | Refactoring and documentation
