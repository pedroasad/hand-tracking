#Hand Tracking
Model-based hand tracking for my Master Thesis. Based on the work

> Oikonomidis, I., Kyriazis, N., & Argyros, A. A. (2011, August). Efficient
> model-based 3D tracking of hand articulations using Kinect. In BMVC (Vol. 1,
> No.  2, p. 3).

Work in progress. This commit only contains prototype Python code in the
`python` subdirectory and this `README` in the project's root.

#Project roadmap

Refer to [this file](ROADMAP.md).

#Acknowledgments
Custom `FindFreenect.cmake` and `FindLibUSB.cmake` modules thanks to [Jordan T
Bates's project on Github](https://github.com/jtbates/lua---kinect). The project
page and contents have no license declaration, so I just copied the code and
mention his name here.

#Contact
[Pedro Asad](http://www.pedroasad.com) (pasad at cos dot ufrj dot br)
