#ifndef VISUALIZEROPTIONS_H_
#define VISUALIZEROPTIONS_H_

#include <msc/OptionsSingleton.h>
#include <stdx/string.h>

namespace lcg {
namespace msc {

class RecorderOptions :
		public OptionsSingleton<RecorderOptions> {

	friend SingletonBase;

public:
	bool active;

protected:
	virtual void registerOptions();
	virtual void validateOptions();
};

} /* namespace lcg::msc */
} /* namespace lcg */


#endif /* VISUALIZEROPTIONS_H_ */
