#ifndef STRING_H_
#define STRING_H_

#include <stdx/basic_string.h>

namespace stdx {

typedef basic_string<char> string;

}

#endif /* STRING_H_ */
