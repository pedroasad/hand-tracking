#ifndef STDX_IO_ERROR_H_
#define STDX_IO_ERROR_H_

#include <stdexcept>
#include <stdx/string.h>

namespace stdx {

class io_error : public std::exception {
public:
	static io_error open_error(const stdx::string &file_path);

	io_error(const io_error&) = default;
	virtual const char* what() const noexcept;

private:
	io_error(const stdx::string &message);

	stdx::string message;
};

} /* namespace stdx */

#endif /* STDX_IO_ERROR_H_ */
