#ifndef STDX_ALGORITHM_H_
#define STDX_ALGORITHM_H_

#include "cudasupport.h"
#include <t3/supertype.h>

#include <initializer_list>

namespace stdx {

template <class TypeA, class TypeB>
HOST_DEVICE_IF_CUDA inline
typename lcg::t3::supertype<TypeA, TypeB>::type
min(const TypeA &a, const TypeB &b) {
	return a < b ? a : b;
}

template <class TypeA, class TypeB>
HOST_DEVICE_IF_CUDA inline
typename lcg::t3::supertype<TypeA, TypeB>::type
max(const TypeA &a, const TypeB &b) {
	return a > b ? a : b;
}

// The definitions below should conform to the C++14 standard, but have not been tested yet
//template <class TypeA, class TypeB>
//constexpr HOST_DEVICE_IF_CUDA
//const typename lcg::t3::supertype<TypeA, TypeB>::type&
//max(const TypeA &a, const TypeB &b) {
//	return a > b ? a : b;
//}
//
//template <class TypeA, class TypeB, class Compare>
//constexpr HOST_DEVICE_IF_CUDA
//const typename lcg::t3::supertype<TypeA, TypeB>::type&
//max(const TypeA &a, const TypeB &b, Compare comp) {
//	return comp(b, a) ? a : b;
//}
//
//template <class Type>
//constexpr HOST_DEVICE_IF_CUDA
//const Type
//max(std::initializer_list<Type> il) {
//	std::initializer_list<Type>::iterator element = il.begin();
//	Type result = *element;
//	while (element != il.end()) {
//		if (*element > result)
//			result = element;
//		element++;
//	}
//	return result;
//}
//
//template <class Type, class Compare>
//constexpr HOST_DEVICE_IF_CUDA
//const Type
//max(std::initializer_list<Type> il, Compare compare) {
//	std::initializer_list<Type>::iterator element = il.begin();
//	Type result = *element;
//	while (element != il.end()) {
//		if (compare(result, *element))
//			result = element;
//		element++;
//	}
//	return result;
//}

} /* namespace stdx */

#endif /* STDX_ALGORITHM_H_ */
