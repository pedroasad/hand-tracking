#ifndef LCG_FSTREAM_H_
#define LCG_FSTREAM_H_

#include <fstream>
#include "basic_string.h"

namespace stdx {

template<class CharT, class Traits = char_traits<CharT> >
class basic_ifstream :
		public std::basic_ifstream<CharT, Traits> {

public:
	using std::basic_ifstream<CharT, Traits>::basic_ifstream;

	basic_string<CharT, Traits> read() {
		std::istreambuf_iterator<CharT> begin(*this);
		std::istreambuf_iterator<CharT> end;

		basic_string<CharT, Traits> contents(begin, end);

		return contents;
	}
};

typedef basic_ifstream<char> ifstream;

} /* namespace stdx */

#endif /* LCG_FSTREAM_H_ */
