#ifndef STDX_MAP_H_
#define STDX_MAP_H_

#include <map>

namespace stdx {

template <
	typename Key,
	typename Value,
	class Compare = std::less<Key>,
	class Alloc = std::allocator<std::pair<const Key, Value>>
> class map : public std::map<Key, Value, Compare, Alloc> {
public:

	using std::map<Key, Value, Compare, Alloc>::map;

//	Value& operator[](const Key &key) {
//		return this->at(key);
//	}
//
//	const Value& operator[](const Key &key) const {
//		return this->at(key);
//	}

	bool has(const Key &key) const {
		return this->find(key) != this->end();
	}
};

} /* namespace stdx */

#endif /* STDX_MAP_H_ */
