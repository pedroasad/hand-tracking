#ifndef LCG_BASIC_STRING_H_
#define LCG_BASIC_STRING_H_

#include <sstream>
#include <string>

namespace stdx {

template<
    class CharT,
    class Traits = std::char_traits<CharT>,
    class Allocator = std::allocator<CharT>
> class basic_string :
		public std::basic_string<CharT, Traits, Allocator> {

public:

	using std::basic_string<CharT, Traits, Allocator>::basic_string;

	basic_string() :
			std::basic_string<CharT, Traits, Allocator>() {
	}

	basic_string(const std::basic_string<CharT, Traits, Allocator> &other) :
			std::basic_string<CharT, Traits, Allocator>(other) {
	}

	operator CharT*() {
		return this->c_str();
	}

	operator const CharT*() const {
		return this->c_str();
	}

	template <class ...Args>
	static basic_string collapse(Args ...arguments) {
		std::basic_stringstream<CharT> buffer;
		collapse_impl(buffer, arguments...);
		return buffer.str();
	}

private:

	template <class FirstArg, class ...MoreArgs>
	static void
	collapse_impl(
			std::basic_stringstream<CharT> &buffer,
			const FirstArg &first_arg,
			MoreArgs ...more_args) {

		buffer << first_arg;
		collapse_impl(buffer, more_args...);
	}

	static void
	collapse_impl(
			std::basic_stringstream<CharT> &buffer) {
	}

//	basic_string format(...) const {
//		va_list args;
//
//		std::map<std::string, int> placeholder_lengths = {
//			{ "%d",
//			{ "%u",
//			{ "%o",
//			{ "%x",
//			{ "%X",
//			{ "%f",
//			{ "%F",
//			{ "%e",
//			{ "%E",
//			{ "%g",
//			{ "%G",
//			{ "%a",
//			{ "%A",
//			{ "%c",
//			{ "%s",
//			{ "%p",
//		};
//
//		int pl_found = 0;
//		for (char *pl : placeholders)
//			std::count(this->begin(), this->end()
//
//		char *buffer = new char[size()];
//	}
};

template<
    class CharT,
    class Traits = std::char_traits<CharT>,
    class Allocator = std::allocator<CharT>
>
basic_string<CharT, Traits, Allocator>
operator*(const basic_string<CharT, Traits, Allocator> &string, int n) {
	std::stringstream result;
	for (int i = 0; i < n; i++)
		result << string;
	return result.str();
}

template<
    class CharT,
    class Traits = std::char_traits<CharT>,
    class Allocator = std::allocator<CharT>
>
basic_string<CharT, Traits, Allocator>
operator*(int n, const basic_string<CharT, Traits, Allocator> &string) {
	std::stringstream result;
	for (int i = 0; i < n; i++)
		result << string;
	return result.str();
}

} /* namespace stdx */

#endif /* LCG_BASIC_STRING_H_ */
