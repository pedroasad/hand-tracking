#ifndef LCG_TEMPLATRICKS_SUPERTYPE_H_
#define LCG_TEMPLATRICKS_SUPERTYPE_H_

#include <stdint.h>

namespace lcg {
namespace t3 {

template <class Type>
struct supertype_base {
	typedef Type type;
};

template <class A, class B>
struct supertype : supertype<B, A> {};

template <class A>
struct supertype<A, A> : supertype_base<A> {};

//============================================================================//
// Supertypes for unsigned char
//============================================================================//
template <>
struct supertype<unsigned char, char> : supertype_base<char> {};

template <>
struct supertype<unsigned char, unsigned short> : supertype_base<unsigned short> {};

template <>
struct supertype<unsigned char, short> : supertype_base<short> {};

template <>
struct supertype<unsigned char, unsigned int> : supertype_base<unsigned int> {};

template <>
struct supertype<unsigned char, int> : supertype_base<int> {};

template <>
struct supertype<unsigned char, unsigned long int> : supertype_base<unsigned long int> {};

template <>
struct supertype<unsigned char, long int> : supertype_base<long int> {};

template <>
struct supertype<unsigned char, unsigned long long int> : supertype_base<unsigned long long int> {};

template <>
struct supertype<unsigned char, long long int> : supertype_base<long long int> {};

template <>
struct supertype<unsigned char, float> : supertype_base<float> {};

template <>
struct supertype<unsigned char, double> : supertype_base<double> {};

template <>
struct supertype<unsigned char, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for char
//============================================================================//
template <>
struct supertype<char, unsigned short> : supertype_base<short> {};

template <>
struct supertype<char, short> : supertype_base<short> {};

template <>
struct supertype<char, unsigned int> : supertype_base<int> {};

template <>
struct supertype<char, int> : supertype_base<int> {};

template <>
struct supertype<char, unsigned long int> : supertype_base<long int> {};

template <>
struct supertype<char, long int> : supertype_base<long int> {};

template <>
struct supertype<char, unsigned long long int> : supertype_base<long long int> {};

template <>
struct supertype<char, long long int> : supertype_base<long long int> {};

template <>
struct supertype<char, float> : supertype_base<float> {};

template <>
struct supertype<char, double> : supertype_base<double> {};

template <>
struct supertype<char, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for unsigned short
//============================================================================//
template <>
struct supertype<unsigned short, short> : supertype_base<short> {};

template <>
struct supertype<unsigned short, unsigned int> : supertype_base<unsigned int> {};

template <>
struct supertype<unsigned short, int> : supertype_base<int> {};

template <>
struct supertype<unsigned short, unsigned long int> : supertype_base<unsigned long int> {};

template <>
struct supertype<unsigned short, long int> : supertype_base<long int> {};

template <>
struct supertype<unsigned short, unsigned long long int> : supertype_base<unsigned long long int> {};

template <>
struct supertype<unsigned short, long long int> : supertype_base<long long int> {};

template <>
struct supertype<unsigned short, float> : supertype_base<float> {};

template <>
struct supertype<unsigned short, double> : supertype_base<double> {};

template <>
struct supertype<unsigned short, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for short
//============================================================================//
template <>
struct supertype<short, unsigned int> : supertype_base<int> {};

template <>
struct supertype<short, int> : supertype_base<int> {};

template <>
struct supertype<short, unsigned long int> : supertype_base<long int> {};

template <>
struct supertype<short, long int> : supertype_base<long int> {};

template <>
struct supertype<short, unsigned long long int> : supertype_base<long long int> {};

template <>
struct supertype<short, long long int> : supertype_base<long long int> {};

template <>
struct supertype<short, float> : supertype_base<float> {};

template <>
struct supertype<short, double> : supertype_base<double> {};

template <>
struct supertype<short, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for unsigned int
//============================================================================//
template <>
struct supertype<unsigned int, int> : supertype_base<int> {};

template <>
struct supertype<unsigned int, unsigned long int> : supertype_base<unsigned long int> {};

template <>
struct supertype<unsigned int, long int> : supertype_base<long int> {};

template <>
struct supertype<unsigned int, unsigned long long int> : supertype_base<unsigned long long int> {};

template <>
struct supertype<unsigned int, long long int> : supertype_base<long long int> {};

template <>
struct supertype<unsigned int, float> : supertype_base<float> {};

template <>
struct supertype<unsigned int, double> : supertype_base<double> {};

template <>
struct supertype<unsigned int, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for int
//============================================================================//
template <>
struct supertype<int, unsigned long int> : supertype_base<long int> {};

template <>
struct supertype<int, long int> : supertype_base<long int> {};

template <>
struct supertype<int, unsigned long long int> : supertype_base<long long int> {};

template <>
struct supertype<int, long long int> : supertype_base<long long int> {};

template <>
struct supertype<int, float> : supertype_base<float> {};

template <>
struct supertype<int, double> : supertype_base<double> {};

template <>
struct supertype<int, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for unsigned long int
//============================================================================//
template <>
struct supertype<unsigned long int, long int> : supertype_base<long int> {};

template <>
struct supertype<unsigned long int, unsigned long long int> : supertype_base<unsigned long long int> {};

template <>
struct supertype<unsigned long int, long long int> : supertype_base<long long int> {};

template <>
struct supertype<unsigned long int, float> : supertype_base<float> {};

template <>
struct supertype<unsigned long int, double> : supertype_base<double> {};

template <>
struct supertype<unsigned long int, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for long int
//============================================================================//
template <>
struct supertype<long int, unsigned long long int> : supertype_base<long long int> {};

template <>
struct supertype<long int, long long int> : supertype_base<long long int> {};

template <>
struct supertype<long int, float> : supertype_base<float> {};

template <>
struct supertype<long int, double> : supertype_base<double> {};

template <>
struct supertype<long int, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for unsigned long long int
//============================================================================//
template <>
struct supertype<unsigned long long int, long long int> : supertype_base<long long int> {};

template <>
struct supertype<unsigned long long int, float> : supertype_base<float> {};

template <>
struct supertype<unsigned long long int, double> : supertype_base<double> {};

template <>
struct supertype<unsigned long long int, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for long long int
//============================================================================//
template <>
struct supertype<long long int, float> : supertype_base<float> {};

template <>
struct supertype<long long int, double> : supertype_base<double> {};

template <>
struct supertype<long long int, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for float
//============================================================================//
template <>
struct supertype<float, double> : supertype_base<double> {};

template <>
struct supertype<float, long double> : supertype_base<long double> {};

//============================================================================//
// Supertypes for double
//============================================================================//
template <>
struct supertype<double, long double> : supertype_base<long double> {};

} /* namespace t3 */
} /* namespace lcg */

#endif /* LCG_TEMPLATRICKS_SUPERTYPE_H_ */
