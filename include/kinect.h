#ifndef LCG_MSC_KINECT_H_
#define LCG_MSC_KINECT_H_

const float KINECT_CLIP_NEAR = 0.4;
const float KINECT_CLIP_FAR  = 0.6;

#endif /* LCG_MSC_KINECT_H_ */
