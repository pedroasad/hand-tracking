#ifndef LCG_CUIMG_NEIGH_H_
#define LCG_CUIMG_NEIGH_H_

#include "image.h"
#include <initializer_list>

namespace lcg {
namespace cuimg {

//TODO: Change from int2 to cutils::vec2i
typedef std::initializer_list<int2> neighborhood;

__host__ __device__ inline
neighborhood
neigh4(const int2 &point = make_int2(0, 0)) {
	return {
		                           point + make_int2(0, -1),
		point + make_int2(-1,  0),                           point + make_int2(1,  0),
		                           point + make_int2(0,  1)
	};
}

__host__ __device__ inline
neighborhood
neigh8(const int2 &point = make_int2(0, 0)) {
	return {
		point + make_int2(-1, -1), point + make_int2(0, -1), point + make_int2(1, -1),
		point + make_int2(-1,  0),                           point + make_int2(1,  0),
		point + make_int2(-1,  1), point + make_int2(0,  1), point + make_int2(1,  1)
	};
}

//TODO: Publish versions for arbitrary neighborhoods and accepting int2/vector arguments
//TODO: These functions may not come in __host__ __device flavor, because the Function parameter might be a __host__ or __device__ only lambda, which generates a compilation error. Find a workaround.
template <class Function>
__device__ inline
void for_8neighbors(int x, int y, int width, int height, Function function) {
	for (auto &n : neigh8())
		if (rectangle(width, height).contains(make_int2(x, y) + n))
			function(x + n.x, y + n.y);
}

template <class Function, class T, int W, int H>
__device__ inline
void for_8neighbors(int x, int y, image<T, W, H> &img, Function function) {
	for (auto &n : neigh8())
		if (image<T, W, H>::domain.contains(make_int2(x, y) + n))
			function(x + n.x, y + n.y);
}

template <class Function, class T, int W, int H>
__device__ inline
void for_8neighbors(int x, int y, Function function) {
	for (auto &n : neigh8())
		function(x + n.x, y + n.y);
}

template <class Function>
__device__ inline
void for_neighbors(int x, int y, const neighborhood &neighbors, Function function) {
	for (auto &n : neighbors)
		function(x + n.x, y + n.y);
}

} /* namespace cuimg */
} /* namespace lcg */

#endif /* LCG_CUIMG_NEIGH_H_ */
