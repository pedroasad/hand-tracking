#ifndef LCG_CUIMG_COLORS_H_
#define LCG_CUIMG_COLORS_H_

#include <stdint.h>

#include <cutils/types.h>

namespace lcg {
namespace cuimg {

using cutils::matrix;
using cutils::vector;

template<class Type = uint8_t>
struct RGB;

template<class Type = uint8_t>
struct YUV;

template<class Type = uint8_t>
struct UV;

template<class Type>
struct RGB : vector<Type, 3> {

	__host__ __device__
	RGB(const matrix<Type, 3, 1> &rgb) :
			vector<Type, 3> (rgb(0), rgb(1), rgb(2)) {
	}

	__host__ __device__
	RGB(Type r = 0, Type g = 0, Type b = 0) :
			vector<Type, 3>(r, g, b) {
	}

	__host__ __device__
	const Type& r() const {
		return this->x();
	}

	__host__ __device__
	Type& r() {
		return this->x();
	}

	__host__ __device__
	const Type& g() const {
		return this->y();
	}

	__host__ __device__
	Type& g() {
		return this->y();
	}

	__host__ __device__
	const Type& b() const {
		return this->z();
	}

	__host__ __device__
	Type& b() {
		return this->z();
	}

	//TODO: The exact conversion matrix depends on the colorspace representation. That is, in 8-bit integer coded channels, translation is indeed 128, but in floating-point-encoded channels, translation factor is 0.5.
	//TODO: One confusing side-effect of inheriting from vector<...> is that the conversion operators must be called explicitly, since assignment from a matrix<...> ref of compatible dimensions is always accepted.
	__host__ __device__
	operator YUV<Type>() const {

		matrix<float, 4, 4> conversion;
		conversion <<
			 0.2990f,  0.5870f,  0.1140f,   0.0f,
			-0.1687f, -0.3313f,  0.5000f, 128.0f,
			 0.5000f, -0.4187f, -0.8130f, 128.0f,
			 0.0000f,  0.0000f,  0.0000f,   1.0f;

		vector<float, 4> rgbw = this->homogeneous_form().template cast<float>();
		vector<float, 4> yuvw = conversion * rgbw;

		return yuvw.homogenize().template cast<Type>();

//		matrix<float, 3, 3> conversion;
//		conversion <<
//			 0.299f, 0.587f,  0.114f,
//			-0.147f, 0.289f,  0.436f,
//			 0.615f, 0.515f, -0.100f;
//
//		vector<float, 3> yuv = conversion * (*this);
//
//		return yuv.template cast<Type>();
	}
};

template<class Type>
struct YUV : vector<Type, 3> {

	__host__ __device__
	YUV(const matrix<Type, 3, 1> &yuv) :
			vector<Type, 3>(yuv(0), yuv(1), yuv(2)) {
	}

	__host__ __device__
	YUV(Type y, const matrix<Type, 2, 1> &uv) : vector<Type, 3>(y, uv(0), uv(1)) {
	}

	__host__ __device__
	YUV(Type y = 0, Type u = 0, Type v = 0) :
			vector<Type, 3>(y, u, v) {
	}

	//TODO: Implementing YUV as a subclass of vector<..., 3> makes method names inconsistent (y for y-coordinate or for Y-component?)
	__host__ __device__
	const Type& y() const {
		return (*this)(0);
	}

	__host__ __device__
	Type& y() {
		return (*this)(0);
	}

	__host__ __device__
	const Type& u() const {
		return (*this)(1);
	}

	__host__ __device__
	Type& u() {
		return (*this)(1);
	}

	__host__ __device__
	const Type& v() const {
		return (*this)(2);
	}

	__host__ __device__
	Type& v() {
		return (*this)(2);
	}

	//TODO: Might be implemented with matrix-vector multiplication
	__host__ __device__
	operator RGB<Type>() const {
		matrix<float, 4, 4> conversion;
		conversion <<
			2.025733300f,  1.81754014f,  1.401849530000f, -412.081878f,
			0.477541683f, -1.26989799f, -0.714033507000f,  253.943231f,
			0.999901535f,  1.77180364f, -0.000134570161f, -226.773641f,
			0.000000000f,  0.00000000f,  0.000000000000f,    1.000000f;

		vector<float, 4> yuvw = this->homogeneous_form().template cast<float>();
		vector<float, 4> rgbw = conversion * yuvw;

		return rgbw.homogenize().template cast<Type>();

//		matrix<float, 3, 3> conversion;
//				conversion <<
//				1.0f,  0.000f,  1.140f,
//				1.0f, -0.147f, -0.581f,
//				1.0f,  2.032f,  0.000f;
//
//		vector<float, 3> rgb = conversion * (*this);
//
//		return rgb.template cast<Type>();
	}

	__host__ __device__
	matrix<Type, 2, 1> uv() const {
		return UV<Type>(u(), v());
	}
};

template<class Type>
struct YUVA : vector<Type, 4> {

	//TODO: All these constructors may probably be removed, by adding the line below
//	using vector<Type, 4>::vector;

	__host__ __device__
	YUVA(const matrix<Type, 4, 1> &yuv) :
			vector<Type, 4>(yuv(0), yuv(1), yuv(2)) {
	}

	__host__ __device__
	YUVA(Type y, const matrix<Type, 4, 1> &uv) : vector<Type, 4>(y, uv(0), uv(1)) {
	}

	__host__ __device__
	YUVA(Type y = 0, Type u = 0, Type v = 0) :
			vector<Type, 4>(y, u, v) {
	}

	//TODO: Implementing YUV as a subclass of vector<..., 3> makes method names inconsistent (y for y-coordinate or for Y-component?)
	__host__ __device__
	const Type& y() const {
		return (*this)(0);
	}

	__host__ __device__
	Type& y() {
		return (*this)(0);
	}

	__host__ __device__
	const Type& u() const {
		return (*this)(1);
	}

	__host__ __device__
	Type& u() {
		return (*this)(1);
	}

	__host__ __device__
	const Type& v() const {
		return (*this)(2);
	}

	__host__ __device__
	Type& v() {
		return (*this)(2);
	}

	//TODO: Might be implemented with matrix-vector multiplication
	__host__ __device__
	operator RGB<Type>() const {
		matrix<float, 4, 4> conversion;
		conversion <<
			2.025733300f,  1.81754014f,  1.401849530000f, -412.081878f,
			0.477541683f, -1.26989799f, -0.714033507000f,  253.943231f,
			0.999901535f,  1.77180364f, -0.000134570161f, -226.773641f,
			0.000000000f,  0.00000000f,  0.000000000000f,    1.000000f;

		vector<float, 4> yuvw = this->xyz()->homogeneous_form().template cast<float>();
		vector<float, 4> rgbw = conversion * yuvw;

		return rgbw.homogenize().template cast<Type>();

//		matrix<float, 3, 3> conversion;
//				conversion <<
//				1.0f,  0.000f,  1.140f,
//				1.0f, -0.147f, -0.581f,
//				1.0f,  2.032f,  0.000f;
//
//		vector<float, 3> rgb = conversion * (*this);
//
//		return rgb.template cast<Type>();
	}

	__host__ __device__
	matrix<Type, 2, 1> uv() const {
		return UV<Type>(u(), v());
	}
};

template<class Type>
struct UV : vector<Type, 2> {

	__host__ __device__
	UV(const UV &uv) :
			vector<Type, 2>(uv.u(), uv.v()) {
	}

	__host__ __device__
	UV(Type u, Type v) :
			vector<Type, 2>(u, v) {
	}

	__host__ __device__
	const Type& u() const {
		return (*this)(1);
	}

	__host__ __device__
	Type& u() {
		return (*this)(1);
	}

	__host__ __device__
	const Type& v() const {
		return (*this)(2);
	}

	__host__ __device__
	Type& v() {
		return (*this)(2);
	}
};

typedef RGB<uint8_t> RGB888;
typedef RGB888 RGB24;

typedef YUV<uint8_t> YUV888;
typedef YUV888 YUV24;

typedef YUVA<uint8_t> YUVA8888;
typedef YUVA8888 YUV32;

} /* cuimg */
} /* lcg */

#endif /* LCG_CUIMG_COLORS_H_ */
