#ifndef LCG_FUNCTIONS_CUIMG_APPLY_H_
#define LCG_FUNCTIONS_CUIMG_APPLY_H_

#include <cutils/thread_index.h>

namespace lcg {
namespace cuimg {

using cutils::thread_index;
using cutils::vec2i;

template <class Type, int W, int H, class Function>
__global__
void apply(image<Type, W, H> &some_image, Function function) {
	const vec2i p = thread_index().xy();
	const int   i = thread_index().plain();

	some_image(p.x(), p.y()) = function(some_image(p.x(), p.y()));
}

} /* namespace cuimg */
} /* namespace lcg */

#endif /* LCG_FUNCTIONS_CUIMG_APPLY_H_ */
