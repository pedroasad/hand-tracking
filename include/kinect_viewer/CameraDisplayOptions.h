#ifndef CAMERADISPLAYOPTIONS_H_
#define CAMERADISPLAYOPTIONS_H_

#include <msc/OptionsSingleton.h>

namespace lcg {
namespace msc {

struct CameraDisplayOptions :
		public OptionsSingleton<CameraDisplayOptions> {

	friend SingletonBase;

protected:
	virtual void registerOptions();
	virtual void validateOptions();
};

}
}

#endif /* CAMERADISPLAYOPTIONS_H_ */
