#ifndef LCG_CUCCL_KERNELS_H_
#define LCG_CUCCL_KERNELS_H_

#include <cuccl/cuccl.h>
#include <cuimg/neigh.h>

namespace lcg {
namespace cuccl {

using cuimg::neighborhood;

__host__ __device__ inline
neighborhood
neigh_scan(const int2 &point = make_int2(0, 0)) {
	return {
		point + make_int2(-1, -1), point + make_int2(0, -1), point + make_int2(1, -1),
		point + make_int2(-1,  0)
	};
}

__host__ __device__ inline
neighborhood
neigh_link(const int2 &point = make_int2(0, 0)) {
	return {
		                                                     point + make_int2(1, -1),
		point + make_int2(-1,  0),                           point + make_int2(1,  0),
		point + make_int2(-1,  1)
	};
}

extern texture<segment_image::element_type, cudaTextureType2D> binaryTex;
extern texture<   full_label::   base_type, cudaTextureType2D>  labelTex;

__device__ inline
full_label
smallest(const neighborhood &neighbors, const int2 &index) {

	full_label smallest = full_label::max();

	for (auto &n : neighbors) {
		const full_label label = tex2D(labelTex, index.x + n.x, index.y + n.y);

		if (label < smallest)
			smallest = label;
	}

	return smallest;
}

__device__
full_label find_root(const full_label_image &table, full_label label);

__global__
void prelabel(full_label_image &labels, properties &props, bool &changed);

__global__
void prelabel(full_label_image &labels, bool &changed);

__global__
void scan(full_label_image &labels, bool &changed);

__global__
void flatten(full_label_image &labels, properties &props);

__global__
void flatten(full_label_image &labels);

} /* namespace cuccl */
} /* namespace lcg */

#endif /* LCG_CUCCL_KERNELS_H_ */
