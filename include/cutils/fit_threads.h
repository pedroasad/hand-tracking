#ifndef LCG_CUTILS_FIT_THREADS_H_
#define LCG_CUTILS_FIT_THREADS_H_

#include <t3/bits.h>

namespace lcg {
namespace cutils {

const int WARP_SIZE = 32;

/**
 * Given a W x H domain and a preferred thread layout X x Y (where X and Y
 * should be powers of 2), computes the thread layout (bx,by) that best
 * satisfies the following criteria, in order:
 *
 * 	1. bx and by should be no greater than the maximum powers of two
 * 	   that divide W and H, respectively
 *	2. bx by is desired to be equal to XY
 *	3. bx by is desired to be no less than the warp size
 *	4. (bx, by) are desired to be (x, Y)
 **/
template <unsigned W, unsigned H, unsigned X = 16, unsigned Y = 16>
struct fit_threads {
	static const dim3 block, grid;

private:
	template <unsigned a, unsigned b>
	struct min {
		static const unsigned value = a < b ? a : b;
	};

	template <unsigned a, unsigned b>
	struct max {
		static const unsigned value = a > b ? a : b;
	};

	static const unsigned bx = min<
		max<
			max<
				X,
				WARP_SIZE / t3::gp2<H>::pow>::value,
			(X * Y) / t3::gp2<H>::pow>::value,
		t3::gp2<W>::pow>::value;

	static const unsigned by = min<
		(X * Y) / bx,
		t3::gp2<H>::pow>::value;
};

template <unsigned W, unsigned H, unsigned X, unsigned Y>
const dim3 fit_threads<W, H, X, Y>::block(bx, by);

template <unsigned W, unsigned H, unsigned X, unsigned Y>
const dim3 fit_threads<W, H, X, Y>::grid(W / bx, H / by);

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUTILS_FIT_THREADS_H_ */
