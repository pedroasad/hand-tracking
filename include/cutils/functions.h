#ifndef LCG_CUDAUTILS_FUNCTIONS_H_
#define LCG_CUDAUTILS_FUNCTIONS_H_

namespace lcg {
namespace cutils {

template <class Type>
__host__ __device__ inline
Type sqr(const Type &k) {
	return k * k;
}

template <class Type>
__host__ __device__ inline
void swap(Type &a, Type &b) {
	Type c = a;
	a = b;
	b = c;
}

template <class Type, int N>
__host__ __device__ inline
size_t length(const Type (&array)[N]) {
	return N;
}

typedef unsigned long long nat_t;

template <class Numeric>
__host__ __device__ inline
Numeric sum(const Numeric &n, const Numeric &x = 0) {
	return x * n + n * (n - 1) / 2;
}

template <class Numeric>
__host__ __device__ inline
Numeric sum_sqr(const Numeric &n, const Numeric &x = 0) {
	return x * x * n + x * n * (n-1) + (2 * n * n * n + 3 * n * n + n) / 6;
}

//TODO: Move this into t3
namespace temp {
// Sum of N natural numbers from
template <nat_t N, nat_t First=0>
struct sum {
	static const nat_t value = sum<N>::value - sum<First - 1>::value;
};

template <nat_t N>
struct sum<N, 0> {
	static const nat_t value = N * (N - 1) / 2;
};

template <nat_t N, nat_t First=0>
struct sum_sqr {
	static const nat_t value = sum_sqr<N>::value - sum_sqr<First - 1>::value;
};

template <nat_t N>
struct sum_sqr<N, 0> {
	static const nat_t value = (N * N * N) / 3 + (N * N) / 2 + N / 6;
};
}

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_FUNCTIONS_H_ */
