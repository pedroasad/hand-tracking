#ifndef LCG_CUTILS_THREAD_INDEX_H_
#define LCG_CUTILS_THREAD_INDEX_H_

#include <cuimg/image.h>
#include <cutils/types/dimensionality.h>
#include <cutils/types/vector.h>

namespace lcg {
namespace cutils {

//TODO: This class should replace cuda_index.
struct thread_index : vec3i {

	using vec3i::vec3i;

	__device__ inline
	thread_index() : vec3i(
			blockIdx.x * blockDim.x + threadIdx.x,
			blockIdx.y * blockDim.y + threadIdx.y,
			blockIdx.z * blockDim.z + threadIdx.z) {
	}

	//TODO: This constructor should be discarded as soon as __device__ construction of vector types has been fixed
	__device__ inline
	thread_index(const int2 &xy) :
			vec3i(xy.x, xy.y, 0) {
	}

	__device__ inline
	int plain() const {
		return dot(vec3i(
			1,
			blockDim.x * gridDim.x,
			blockDim.x * gridDim.x * blockDim.y * gridDim.y
		));
	}
};

template<>
struct dimensionality<thread_index> {
	static const int value = 1;
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUTILS_THREAD_INDEX_H_ */
