#ifndef LCG_CUDAUTILS_SHARED_KERNEL_PTR_H_
#define LCG_CUDAUTILS_SHARED_KERNEL_PTR_H_

#include <cutils/cuda_exception.h>
#include <cutils/shared_device_ptr.h>

namespace lcg {
namespace cutils {

template<typename Type>
class shared_kernel_ptr {
private:
	Type *data;

public:
	shared_kernel_ptr(Type* data) :
			data(data) {
	}

	shared_kernel_ptr(const shared_kernel_ptr<Type>& other) :
			data(other.data) {
	}

	shared_kernel_ptr(const shared_device_ptr<Type>& other) :
			data(other.data) {
	}

	__device__
	shared_kernel_ptr<Type>& operator=(const shared_kernel_ptr<Type>& other) {
		if (this != &other)
			data = other.data;
		return *this;
	}

	__device__
	operator Type*() {
		return data;
	}

	__device__
	operator const Type*() const {
		return data;
	}

	__device__
	Type* operator->() {
		return data;
	}

	__device__
	const Type* operator->() const {
		return data;
	}

	__device__
	Type& operator*() {
		return *data;
	}

	__device__
	const Type& operator*() const {
		return *data;
	}
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_SHARED_PTR_KERNEL_H_ */
