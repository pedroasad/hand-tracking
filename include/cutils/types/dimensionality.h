#ifndef LCG_CUTILS_TYPES_DIMENSIONALITY_H_
#define LCG_CUTILS_TYPES_DIMENSIONALITY_H_

#include <initializer_list>

namespace lcg {
namespace cutils {

/**
 * \brief Meta-structure to determine at compile-time how many degrees of
 * indexing a certain data type offers.
 *
 * For instance, primitive scalar types have dimensionality 0, vectors have
 * dimensionality 1, matrices have dimensionality 2, and so on. Template
 * specializations for this meta-structure are defined in different files. This
 * header file only contains specializations for primitive scalar types.
 * Specializations for \class matrix, \class vector and CUDA built-in vector
 * types may be found in files \file matrix.h, \file vector.h and \file
 * cuda_vector.h, respectively.
 */
template <class Type>
struct dimensionality {};

template <class Type, int N>
struct dimensionality<Type[N]> {
	static const int value = 1 + dimensionality<Type>::value;
};

//TODO: Add dimensionality specializations for std::pair and std::tuple as well
template <class Type>
struct dimensionality<std::initializer_list<Type>> {
	static const int value = 1 + dimensionality<Type>::value;
};

template<>
struct dimensionality<char> {
	static const int value = 0;
};

template<>
struct dimensionality<unsigned char> {
	static const int value = 0;
};

template<>
struct dimensionality<short> {
	static const int value = 0;
};

template<>
struct dimensionality<unsigned short> {
	static const int value = 0;
};

template<>
struct dimensionality<int> {
	static const int value = 0;
};

template<>
struct dimensionality<unsigned int> {
	static const int value = 0;
};

template<>
struct dimensionality<long> {
	static const int value = 0;
};

template<>
struct dimensionality<unsigned long> {
	static const int value = 0;
};

template<>
struct dimensionality<long long> {
	static const int value = 0;
};

template<>
struct dimensionality<unsigned long long> {
	static const int value = 0;
};

template<>
struct dimensionality<float> {
	static const int value = 0;
};

template<>
struct dimensionality<double> {
	static const int value = 0;
};

template<>
struct dimensionality<long double> {
	static const int value = 0;
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUTILS_TYPES_DIMENSIONALITY_H_ */
