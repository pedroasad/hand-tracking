#ifndef LCG_CUDAUTILS_TYPES_VECTOR_CUH_
#define LCG_CUDAUTILS_TYPES_VECTOR_CUH_

#include "cuda_vector.h"
#include "matrix.h"

namespace lcg {
namespace cutils {

template <class Scalar, int N>
struct vector;

//TODO: fix and improve this template function. It currently does not work well with cuda vector types, as it claims to do. using range-based loops could prove worth it
/**
 * \brief Converts any CUDA built-in vector objects (int3, float2, etc.) into
 * the corresponding \class matrix object. It also serves as a no-OP to \class
 * matrix objects.
 *
 * Since \class vector is meant to be compatible with \class matrix, \class
 * matrix object are returned, despite the function's name.
 *
 * This template function relies on the fact that \class vector specializations
 * for dimensions 1, 2, 3 and 4 always have a constructor that accepts the
 * corresponding CUDA vector object. The corresponding \class vector type may be
 * determined by applying \class cuda_vector_traits on the argument type to deduce
 * template parameters for \class vector.
 *
 * The `N` template parameter is a dummy parameter to allow specializing this
 * template for \class matrix types.
 *
 * @param some_vector an object for which \class cuda_vector_traits has a
 * specialization (any of CUDA built-in vector types, such as int3, float2, etc.)
 * @return
 */
template <class CudaVector, int N = 0>
__host__ __device__
matrix<typename cuda_vector_traits<CudaVector>::base_type, cuda_vector_traits<CudaVector>::dims, 1>
make_vector(const CudaVector &some_vector) {
	typedef vector<
		typename cuda_vector_traits<CudaVector>::base_type,
		         cuda_vector_traits<CudaVector>::dims
	 > result_type;

	return result_type(some_vector);
}

template <class Scalar, int N>
__host__ __device__
matrix<Scalar, N, 1>
make_vector(const matrix<Scalar, N, 1> &some_vector) {
	return some_vector;
}

//TODO: Since we're using C++11, it could be really worth it using template aliases to declare vector as an alias to matrix, although this might still require a vector_base to avoid repeating constructor code around.
//TODO: Consider implementing the iterator protocol for vector type.
template <class Scalar, int N>
struct vector_base : matrix<Scalar, N, 1> {
	//TODO: Consider how to implement a range check, e.g. it should be impossible to forward more parameters than the vector size. Extra size should not be automatically stripped.
	/**
	 * \brief Helper class that allows constructing vectors with any sequence of
	 * \class matrix_base, scalars, or CUDA built-in vector types.
	 *
	 * This template makes considerable usage of variadic templates to consume
	 * arguments. Different consuming meta-classes are used according to argument
	 * dimensionality, which is obtained from the \class dimensionality meta-class.
	 */
	template <int CurrentIndex = 0>
	struct vector_initializer {

		template <int FirstDim, class FirstValue, class ...MoreValues>
		struct forward_values {};

		template <class FirstValue, class ...MoreValues>
		struct consume_scalar {

			__host__ __device__
			consume_scalar(
					vector_base<Scalar, N> &target_vector,
					const FirstValue &first_value,
					MoreValues ...more_values) {

				target_vector(CurrentIndex) = first_value;

				if (CurrentIndex < N)
					vector_initializer<CurrentIndex + 1>(
							target_vector, more_values...);
			}
		};

		template <class FirstValue, class ...MoreValues>
		struct consume_vector {

			template <int M>
			__host__ __device__
			consume_vector(
					vector_base<Scalar, N> &target_vector,
					const matrix_base<Scalar, M, 1> &first_value,
					MoreValues ...more_values) {

				for (int i = 0; i < M and i < N - CurrentIndex; i++)
					target_vector(CurrentIndex + i) = first_value(i);

				if (CurrentIndex + M < N)
					vector_initializer<CurrentIndex + M>(
							target_vector, more_values...);
			}
		};

		template <class FirstValue, class ...MoreValues>
		struct forward_values<0, FirstValue, MoreValues...> {

			__host__ __device__
			forward_values(
					vector_base<Scalar, N> &target_vector,
					const FirstValue &first_value,
					MoreValues ...more_values) {

				consume_scalar<FirstValue, MoreValues...>(
						target_vector, first_value, more_values...);
			}
		};

		template <class FirstValue, class ...MoreValues>
		struct forward_values<1, FirstValue, MoreValues...> {

			__host__ __device__
			forward_values(
					vector_base<Scalar, N> &target_vector,
					const FirstValue &first_value,
					MoreValues ...more_values) {

				consume_vector<FirstValue, MoreValues...>(
						target_vector, make_vector(first_value), more_values...);
			}
		};

		template <class FirstValue, class ...MoreValues>
		__host__ __device__
		vector_initializer(
				vector_base<Scalar, N> &target_vector,
				const FirstValue &first_value,
				MoreValues ...more_values) {

			forward_values<dimensionality<FirstValue>::value, FirstValue, MoreValues...>(
					target_vector, first_value, more_values...);
		}

		__host__ __device__
		vector_initializer(vector_base<Scalar, N> &target_vector) {

			for (int i = CurrentIndex; i < N; i++)
				target_vector(i) = 0;
		}
	};

	/**
	 * \brief Constructs a vector from any sequence of \class matrix_base, CUDA
	 * built-in vector types or scalars.
	 *
	 * This constructor accepts any amount of compatible parameters, in any
	 * order. If not all coordinates are filled by given parameters, remaining
	 * coordinates are set to zero. This also works as an empty constructor,
	 * which results in a null-vector.
	 *
	 * Any type for which the meta-class \class dimensionality has any
	 * specialization is a compatible parameter.
	 *
	 * \todo Insert some code samples to show the power of this constructor.
	 *
	 * \note This constructor is <b>extensible</b>, by specializing the \class
	 * dimensionality meta-structure.
	 *
	 * @param values
	 */
	template <class ...Values>
	__host__ __device__
	vector_base(Values ...values) {
		vector_initializer<>(*this, values...);
	}

	//TODO: This initializer_list-based constructor is not needed, since the generic constructor accepts initializer_lists (thanks to specializations of dimensionality). Port this beautiful constructor to matrix, as well.
//	__host__ __device__
//	vector_base(const std::initializer_list<Scalar> &elements) {
//
//		#ifndef __CUDA_ARCH__
//		if (elements.size() != N)
//			throw std::logic_error("The number of rows in the initializer_list passed to vector constructor does not match the vector's");
//		#endif
//
//		int i = 0;
//		for (auto &coef : elements) {
//			(*this)(i++) = coef;
//		}
//	}

	/**
	 * \brief Constructs a vector from a given data buffer.
	 *
	 * \note Exactly `N` elements are read from the data buffer.
	 *
	 * @param data pointer to sequential data.
	 */
	__host__ __device__
	vector_base(const Scalar *data) {
		for (int i = 0; i < N; i++)
			(*this)(i) = data[i];
	}

	/**
	 * \brief Constructs a vector from a given data array.
	 *
	 * @param data array of values.
	 */
	__host__ __device__
	vector_base(const Scalar (&data)[N]) {
		for (int i = 0; i < N; i++)
			(*this)(i) = data[i];
	}
};

/**
 * \note This class may be gracefully cast to corresponding CUDA vector type, but the contrary is not true. Maybe the best solution is to use CRTP in the matrix hierarchy to make small vectors inherit from CUDA vector types.
 */
template <class Scalar, int N>
struct vector : vector_base<Scalar, N> {};

//TODO: What to make of this? If vectors have dimensionality 1, then matrix<..., 1, 1> should have dimensionality 0. But that would break vector_initializer code. Unless, perhaps, these special types can be made automagically convertible to the underlying scalar type, which I supspect should work just fine.
template <class Scalar, int N>
struct dimensionality<vector<Scalar, N>> {
	static const int value = 1;
};

template <class Scalar>
struct dimensionality<vector<Scalar, 1>> {
	static const int value = 0;
};

template <class Scalar, int N>
struct dimensionality<vector_base<Scalar, N>> {
	static const int value = 1;
};

template <class Scalar>
struct dimensionality<vector_base<Scalar, 1>> {
	static const int value = 0;
};

template <class Scalar>
struct vector<Scalar, 4> : vector_base<Scalar, 4> {
	//TODO: Add copy constructors for matrix types

	using vector_base<Scalar, 4>::vector_base;

	__host__ __device__
	matrix<Scalar, 3, 1>
	homogenize() const {
		return xyz() / w();
	}

	__host__ __device__
	const Scalar&
	x() const {
		return (*this)(0);
	}

	__host__ __device__
	Scalar&
	x() {
		return (*this)(0);
	}

	__host__ __device__
	const Scalar&
	y() const {
		return (*this)(1);
	}

	__host__ __device__
	Scalar&
	y() {
		return (*this)(1);
	}

	__host__ __device__
	const Scalar&
	z() const {
		return (*this)(2);
	}

	__host__ __device__
	Scalar&
	z() {
		return (*this)(2);
	}

	__host__ __device__
	const Scalar&
	w() const {
		return (*this)(3);
	}

	__host__ __device__
	Scalar&
	w() {
		return (*this)(3);
	}

	__host__ __device__
	matrix<Scalar, 3, 1>
	xyz() const {
		return this->template block<3, 1, 0, 0>();
	}

	__host__ __device__
	matrix<Scalar, 3, 1>
	yzw() const {
		return this->template block<3, 1, 1, 0>();
	}

	__host__ __device__
	matrix<Scalar, 2, 1>
	xy() const {
		return this->template block<2, 1, 0, 0>();
	}

	__host__ __device__
	matrix<Scalar, 2, 1>
	yz() const {
		return this->template block<2, 1, 1, 0>();
	}

	__host__ __device__
	matrix<Scalar, 2, 1>
	zw() const {
		return this->template block<2, 1, 2, 0>();
	}
};

template <class Scalar>
struct vector<Scalar, 3> : vector_base<Scalar, 3> {

	using vector_base<Scalar, 3>::vector_base;

	__host__ __device__
	matrix<Scalar, 4, 1>
	homogeneous_form() const {
		return vector<Scalar, 4>(*this, 1);
	}

	__host__ __device__
	const Scalar&
	x() const {
		return (*this)(0);
	}

	__host__ __device__
	Scalar&
	x() {
		return (*this)(0);
	}

	__host__ __device__
	const Scalar&
	y() const {
		return (*this)(1);
	}

	__host__ __device__
	Scalar&
	y() {
		return (*this)(1);
	}

	__host__ __device__
	const Scalar&
	z() const {
		return (*this)(2);
	}

	__host__ __device__
	Scalar&
	z() {
		return (*this)(2);
	}

	__host__ __device__
	matrix<Scalar, 2, 1>
	xy() const {
		return this->template block<2, 1, 0, 0>();
	}

	__host__ __device__
	matrix<Scalar, 2, 1>
	yz() const {
		return this->template block<2, 1, 1, 0>();
	}
};

template <class Scalar>
struct vector<Scalar, 2> : vector_base<Scalar, 2> {

	using vector_base<Scalar, 2>::vector_base;

	__host__ __device__
	const Scalar&
	x() const {
		return (*this)(0);
	}

	__host__ __device__
	Scalar&
	x() {
		return (*this)(0);
	}

	__host__ __device__
	const Scalar&
	y() const {
		return (*this)(1);
	}

	__host__ __device__
	Scalar&
	y() {
		return (*this)(1);
	}
};

template <class Scalar>
struct vector<Scalar, 1> : vector_base<Scalar, 1> {

	using vector_base<Scalar, 1>::vector_base;

	__host__ __device__
	const Scalar&
	x() const {
		return (*this)(0);
	}

	__host__ __device__
	Scalar&
	x() {
		return (*this)(0);
	}
};

//TODO: Publish more useful typedefs such as these
typedef vector<unsigned, 1> vec1u;
typedef vector<unsigned, 2> vec2u;
typedef vector<unsigned, 3> vec3u;
typedef vector<unsigned, 4> vec4u;

typedef vector<int, 1> vec1i;
typedef vector<int, 2> vec2i;
typedef vector<int, 3> vec3i;
typedef vector<int, 4> vec4i;

typedef vector<unsigned long, 1> vec1ul;
typedef vector<unsigned long, 2> vec2ul;
typedef vector<unsigned long, 3> vec3ul;
typedef vector<unsigned long, 4> vec4ul;

typedef vector<long, 1> vec1l;
typedef vector<long, 2> vec2l;
typedef vector<long, 3> vec3l;
typedef vector<long, 4> vec4l;

typedef vector<float, 1> vec1f;
typedef vector<float, 2> vec2f;
typedef vector<float, 3> vec3f;
typedef vector<float, 4> vec4f;

typedef vector<double, 1> vec1d;
typedef vector<double, 2> vec2d;
typedef vector<double, 3> vec3d;
typedef vector<double, 4> vec4d;

//TODO: Consider the usefulness of template aliases such as
//template <int N>
//using veci = vector<int, N>;

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_TYPES_VECTOR_CUH_ */
