#ifndef LCG_CUDAUTILS_WRAPPERS_H_
#define LCG_CUDAUTILS_WRAPPERS_H_

#include "wrappers/hosted.h"
#include "wrappers/device.h"
#include "wrappers/pinned.h"
#include "wrappers/hosted_ref.h"
#include "wrappers/device_ref.h"
#include "wrappers/kernel_ref.h"

namespace lcg {
namespace cutils {

//TODO: Missing array version
template <class Type>
hosted_ref<Type> in_host(Type &data) {
	return hosted_ref<Type>(data);
}

//TODO: Missing array version
template <class Type>
device_ref<Type> in_device(Type &data) {
	return hosted_ref<Type>(data);
}

/*
Proposals for new templates:

namespace memory {
	enum space {
		CPU, GPU, PIN, CPU_HEAP, CPU_STACK
	};

	enum wrapper {
		ARR, KRN, REF, VAR
	};
};

template<class Derived, memory::space Mem, class Type>
struct memory_wrapper {
};

//Alocates space for an array of the target type. Perhaps, an additional parameter should be added to the others, reducing the number of subclasses and allowing krn to deal with arrays too
template<memory::space Mem, class Type, int N = -1>
struct arr : public memory_wrapper<arr<Mem, Type, N>, Mem, Type*> {
};

//Wrapper meant to be used inside kernels?
template<class Type>
struct krn : public memory_wrapper<krn<Type>, GPU, Type> {
};

//Creates a pointer for the target type
template<memory::space Mem, class Type>
struct ref : public memory_wrapper<ref<Mem, type>, Type> {
};

//Allocates space for the target type
template<memory::space Mem, class Type>
struct var : public memory_wrapper<var<Mem, Type>, Type> {
};
*/

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_WRAPPERS_H_ */
