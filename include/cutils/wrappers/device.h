#ifndef LCG_CUDAUTILS_WRAPPERS_DEVICE_H_
#define LCG_CUDAUTILS_WRAPPERS_DEVICE_H_

#include <cutils/shared_device_ptr.h>

namespace lcg {
namespace cutils {

template<class Type>
class hosted;

template<class Type>
class hosted_ref;

template<class Type>
class pinned;

template<class Type>
class device;

template<class Type>
class device_ref;

template<class Type>
class kernel_ref;

template<class Type>
class device {
	friend class kernel_ref<Type>;
	friend class device_ref<Type>;

	shared_device_ptr<Type> gpupointer;

public:
	device() :
			gpupointer(MEMORY_DEVICE) {
	}

	device(const Type &data) :
			gpupointer(MEMORY_DEVICE) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &data, sizeof(Type), cudaMemcpyHostToDevice) );
	}

	device(const hosted<Type> &other) :
			gpupointer(MEMORY_DEVICE) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
	}

	device(const hosted_ref<Type> &other) :
			gpupointer(MEMORY_DEVICE) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
	}

	device(const pinned<Type> &other) :
			gpupointer(MEMORY_DEVICE) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
	}

	device(const device &other) :
			gpupointer(MEMORY_DEVICE) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyDeviceToDevice) );
	}

	device(const device_ref<Type> &other) :
			gpupointer(MEMORY_DEVICE) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyDeviceToDevice) );
	}

	device& operator= (const Type &other) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
		return *this;
	}

	device& operator= (const hosted<Type> &other) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
		return *this;
	}

	device& operator= (const hosted_ref<Type> &other) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
		return *this;
	}

	device& operator= (const pinned<Type> &other) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyHostToDevice) );
		return *this;
	}

	device& operator= (const device &other) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyDeviceToDevice) );
		return *this;
	}

	device& operator= (const device_ref<Type> &other) {
		CUDA_CHECK( cudaMemcpy(gpupointer, &other, sizeof(Type), cudaMemcpyDeviceToDevice) );
		return *this;
	}

	void memset(int value = 0, size_t bytes = sizeof(Type)) {
		CUDA_CHECK( cudaMemset(gpupointer, value, bytes) );
	}

	operator Type&() {
		return *gpupointer;
	}

	operator const Type&() const {
		return *gpupointer;
	}

	Type& operator*() {
		return *gpupointer;
	}

	const Type& operator*() const {
		return *gpupointer;
	}

	Type* operator->() {
		return gpupointer;
	}

	const Type* operator->() const {
		return gpupointer;
	}

	Type* operator&() {
		return gpupointer;
	}

	const Type* operator&() const {
		return gpupointer;
	}
};

template<class Type>
class device<Type*> {
	friend class hosted<Type*>;
	friend class device_ref<Type*>;
	friend class kernel_ref<Type*>;

	size_t length;
	shared_device_ptr<Type> gpupointer;

public:
	//TODO: Lots of missing constructors

	device(const hosted<Type*> &other) :
			length(other.length), gpupointer(MEMORY_DEVICE, other.length) {
		CUDA_CHECK( cudaMemcpy(gpupointer, other, length * sizeof(Type), cudaMemcpyHostToDevice) );
	}

	device(size_t length) :
			length(length), gpupointer(MEMORY_DEVICE, length) {
	}

	__host__ __device__
	operator Type*() {
		return gpupointer;
	}

	__host__ __device__
	operator const Type*() const {
		return gpupointer;
	}
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_WRAPPERS_DEVICE_H_ */
