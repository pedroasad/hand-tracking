#ifndef LCG_CUDAUTILS_WRAPPERS_PINNED_H_
#define LCG_CUDAUTILS_WRAPPERS_PINNED_H_

#include <cutils/cuda_exception.h>
#include <cutils/shared_device_ptr.h>

namespace lcg {
namespace cutils {

template<class Type>
class hosted;

template<class Type>
class hosted_ref;

template<class Type>
class pinned;

template<class Type>
class device;

template<class Type>
class device_ref;

template <typename Type>
class kernel_ref;

template<class Type>
class pinned {
	friend class kernel_ref<Type>;

	shared_device_ptr<Type> buffer;

public:
	pinned() :
			buffer(MEMORY_PINNED) {
		*buffer = Type();
	}

	pinned(const Type &data) :
			buffer(MEMORY_PINNED) {
		*buffer = data;
	}

	pinned(const pinned &other) :
			buffer(other.buffer) {
		*buffer = other;
	}

	pinned(const hosted<Type> &other) :
			buffer(MEMORY_PINNED) {
		*buffer = other;
	}

	pinned(const hosted_ref<Type> &other) :
			buffer(MEMORY_PINNED) {
		*buffer = other;
	}

	pinned(const device<Type> &other) :
			buffer(MEMORY_PINNED) {
		CUDA_CHECK( cudaMemcpy(buffer, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
	}

	pinned(const device_ref<Type> &other) :
			buffer(MEMORY_PINNED) {
		CUDA_CHECK( cudaMemcpy(buffer, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
	}

	pinned& operator= (const Type &other) {
		*buffer = other;
		return *this;
	}

	pinned& operator= (const hosted<Type> &other) {
		*buffer = other;
		return *this;
	}

	pinned& operator= (const hosted_ref<Type> &other) {
		*buffer = other;
		return *this;
	}

	pinned& operator= (const pinned &other) {
		*buffer = other;
		return *this;
	}

	pinned& operator= (const device<Type> &other) {
		CUDA_CHECK( cudaMemcpy(buffer, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
		return *this;
	}

	pinned& operator= (const device_ref<Type> &other) {
		CUDA_CHECK( cudaMemcpy(buffer, &other, sizeof(Type), cudaMemcpyDeviceToHost) );
		return *this;
	}

	operator Type&() {
		return *buffer;
	}

	operator const Type&() const {
		return *buffer;
	}

	Type& operator*() {
		return *buffer;
	}

	const Type& operator*() const {
		return *buffer;
	}

	Type* operator->() {
		return buffer;
	}

	const Type* operator->() const {
		return buffer;
	}

	Type* operator&() {
		return buffer;
	}

	const Type* operator&() const {
		return buffer;
	}
};

//TODO: Missing pointer/array versions

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_WRAPPERS_PINNED_H_ */
