/**
 * \brief Presents a class for CUDA-specific exceptions and some macros to check against errors in driver API calls.
 */
#ifndef LCG_CUDAUTILS_CUDA_EXCEPTION_H_
#define LCG_CUDAUTILS_CUDA_EXCEPTION_H_

#include <cuda_runtime.h>
#include <iostream>
#include <string>

#define CUDA_CHECK(call) {\
	cudaError_t status = call;\
	if (status != cudaSuccess)\
		throw lcg::cutils::cuda_exception(#call, status);\
}

#define CUDA_DEBUG(call) {\
	cudaError_t status = call;\
	if (status != cudaSuccess)\
		printf("%s:%d: Cuda error %d: %s\n", __FILE__, __LINE__, status, cudaGetErrorString(status));\
}

#define CUDA_FATAL(call) {\
	try {\
		CUDA_CHECK(call);\
	} catch (lcg::cutils::cuda_exception &ex) {\
		std::cerr << ex.what() << std::endl;\
	}\
}

namespace lcg {
namespace cutils {

/**
 * Describes errors related to the CUDA runtime API functions.
 */
class cuda_exception {
public:
	/**
	 * Constructs an exception containing specific information on the source file and line number in which the exception was detected.
	 * @param file The source file that originated the exception
	 * @param line The line where the troubling call was made
	 * @param status CUDA runtime API error code
	 */
	cuda_exception(std::string file, int line, cudaError_t status);

	/**
	 * Constructs an exception informing the CUDA runtime API call that originated the error.
	 * @param call A string representation of the call where the error was detected (function name and arguments given).
	 * @param status CUDA runtime API error code
	 */
	cuda_exception(std::string call, cudaError_t status);

	virtual ~cuda_exception();

	virtual const char* what() const throw ();
	std::string getFile() const;
	int getLine() const;
	cudaError_t getStatus() const;
	std::string getCall() const;

private:
	std::string _call;
	std::string _file;
	int _line;
	cudaError_t _status;
	std::string _what;
};

} /* namespace cutils */
} /* namespace lcg */

#endif /* LCG_CUDAUTILS_CUDA_EXCEPTION_H_ */
