#ifndef LCG_MSC_GLFWRENDERTARGET_H_
#define LCG_MSC_GLFWRENDERTARGET_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <opencv2/core/core.hpp>

namespace lcg {
namespace msc {

class GlfwRenderTarget {
	GLFWwindow *main_window;

public:
	GlfwRenderTarget();
	~GlfwRenderTarget();

	void forceContext();
	GLFWwindow* getWindow();
	void swapBuffers();

	void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
};

} /* namespace lcgmsc */
} /* namespace lcg */

#endif /* LCG_MSC_GLFWRENDERTARGET_H_ */
