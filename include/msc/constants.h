#ifndef CONSTANTS_H_
#define CONSTANTS_H_

namespace lcg {
namespace msc {

//TODO: Make segment class enclose an enumerated type?
const int segment_depth = 3, segment_low = 2, segment_high = 1;
const int max_blobs = 16;
const int min_blob_size = 1000;

} /* namespace msc */
} /* namespace lcg */

#endif /* CONSTANTS_H_ */
