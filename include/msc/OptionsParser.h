#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <msc/OptionsSingleton.h>
#include <stdx/vector.h>
#include <stdx/string.h>

namespace lcg {
namespace msc {

struct OptionsParser :
		t3::SingletonClass<OptionsParser> {

	friend SingletonBase;

	static stdx::string DEFAULT_CONFIG_FILE();

	boost::program_options::options_description configOptions;
	stdx::vector<boost::filesystem::path> configFiles;

	void addConfigurable(OptionsSingletonBase *configurable);
	void parse(int &argc, char **argv);

private:
	OptionsParser();

	boost::program_options::variables_map configVariables;
	stdx::vector<OptionsSingletonBase*> configurables;
};

} /* namespace msc */
} /* namespace lcg*/

#endif /* CONFIGURATION_H_ */
