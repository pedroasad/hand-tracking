#ifndef PREPROCESSOROPTIONS_H_
#define PREPROCESSOROPTIONS_H_

#include <msc/OptionsSingleton.h>

namespace lcg {
namespace msc {

class PreprocessorOptions :
		public OptionsSingleton<PreprocessorOptions> {

	friend SingletonBase;

protected:
	virtual void registerOptions();
	virtual void validateOptions();

public:
	int medianBlurRadius;
};

} /* namespace lcg::msc */
} /* namespace lcg */

#endif /* PREPROCESSOROPTIONS_H_ */
