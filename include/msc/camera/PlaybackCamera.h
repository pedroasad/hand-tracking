#ifndef PLAYBACKCAMERA_H_
#define PLAYBACKCAMERA_H_

#include <msc/camera/AbstractCamera.h>
#include <stdx/vector.h>
#include <stdx/string.h>

namespace lcg {
namespace msc {

class PlaybackCamera:
		public AbstractCamera {

	friend class CameraFactory;

	PlaybackCamera();

	stdx::vector<stdx::string> depthFiles;
	stdx::vector<stdx::string> videoFiles;

	stdx::vector<stdx::string>::const_iterator currentDepthFile;
	stdx::vector<stdx::string>::const_iterator currentVideoFile;

	bool paused;

public:
	bool isPaused();

	void nextFrame();
	void pause();
	void play();
	virtual void run();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* PLAYBACKCAMERA_H_ */
