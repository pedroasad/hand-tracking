#ifndef CAMERAFACTORY_H_
#define CAMERAFACTORY_H_

#include <msc/camera/AbstractCamera.h>
#include <t3/SingletonClass.h>

namespace lcg {
namespace msc {

class CameraFactory :
		public t3::SingletonClass<CameraFactory> {

	friend SingletonBase;

	CameraFactory();
	AbstractCamera *theguy;

public:
	AbstractCamera* make();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* CAMERAFACTORY_H_ */
