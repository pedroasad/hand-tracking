#ifndef CLOCKOPTIONS_H_
#define CLOCKOPTIONS_H_

#include <msc/OptionsSingleton.h>

namespace lcg {
namespace msc {

class ClockOptions :
		public OptionsSingleton<ClockOptions> {

	friend SingletonBase;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* CLOCKOPTIONS_H_ */
