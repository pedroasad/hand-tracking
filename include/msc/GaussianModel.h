#ifndef LCG_MSC_GAUSSIAN_MODEL_H_
#define LCG_MSC_GAUSSIAN_MODEL_H_

#include <cutils/types/matrix.h>
#include <cutils/types/vector.h>
#include <math_constants.h>

namespace lcg {
namespace msc {

using cutils::mat2f;
using cutils::vec2f;

struct GaussianModel {

	vec2f mean;
	mat2f covariance;

	__host__ __device__
	GaussianModel(const GaussianModel &model);

	__host__ __device__
	GaussianModel(const vec2f &mean, const mat2f &covariance);

	__host__ __device__
	float density(const vec2f &point) const;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* LCG_MSC_GAUSSIAN_MODEL_H_ */
