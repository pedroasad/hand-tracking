#ifndef PSODEBUGGERSETTINGS_H_
#define PSODEBUGGERSETTINGS_H_

#include <msc/OptionsSingleton.h>

namespace lcg {
namespace msc {

struct PSODebuggerOptions :
		OptionsSingleton<PSODebuggerOptions> {

	friend SingletonBase;

	bool active;
	bool showDepthDifference, showRenderMatches, showSkinAndMatches, showSkinOrMatches;
	int shownParticles;
	float depthDifferenceBrightnessCorrectionFactor;

protected:
	virtual void registerOptions();
	virtual void validateOptions();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* PSODEBUGGERSETTINGS_H_ */
