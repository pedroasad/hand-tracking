/*
 * RendererOptions.h
 *
 *  Created on: 23/03/2016
 *      Author: pasad
 */

#ifndef RENDEREROPTIONS_H_
#define RENDEREROPTIONS_H_

#include <msc/OptionsSingleton.h>

namespace lcg {
namespace msc {

class RendererOptions :
		public OptionsSingleton<RendererOptions> {

	friend SingletonBase;

protected:
	virtual void registerOptions();
	virtual void validateOptions();

public:
	float depthCorrection;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* RENDEREROPTIONS_H_ */
