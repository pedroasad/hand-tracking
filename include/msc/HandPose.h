#ifndef LCG_MSC_HAND_MODEL_HANDPOSE_H_
#define LCG_MSC_HAND_MODEL_HANDPOSE_H_

#include <Eigen/Dense>
#include <stdx/vector.h>
#include <msc/HandModel.h>

#include <algorithm>
#include <fstream>

namespace Eigen {

template <class Scalar, int Rows, int Options = AutoAlign, int MaxRows = Rows>
using Vector = Eigen::Matrix<Scalar, Rows, 1, Options, MaxRows, 1>;

template <class T, int R, int C, int O, int M, int N>
Matrix<T, R, C, O, M, N>
min(const Matrix<T, R, C, O, M, N> &a, const Matrix<T, R, C, O, M, N> &b) {
	Matrix<T, R, C, O, M, N> c;
	for (int i = 0; i < a.rows(); i++)
		for (int j = 0; j < a.cols(); j++)
			c(i, j) = std::min(a(i, j), b(i, j));
	return c;
}

template <class T, int R, int C, int O, int M, int N>
Matrix<T, R, C, O, M, N>
max(const Matrix<T, R, C, O, M, N> &a, const Matrix<T, R, C, O, M, N> &b) {
	Matrix<T, R, C, O, M, N> c;
	for (int i = 0; i < a.rows(); i++)
		for (int j = 0; j < a.cols(); j++)
			c(i, j) = std::max(a(i, j), b(i, j));
	return c;
}

} /* namespace Eigen */

namespace lcg {
namespace msc {

const int HAND_DOFS = 27;

enum class HandDofs {
	HAND_ROT_X = 0,
	HAND_ROT_Y,
	HAND_ROT_Z,
	HAND_ROT_W,
	HAND_POS_X,
	HAND_POS_Y,
	HAND_POS_Z,

	THUMB_CARPOMETACARPAL_JOINT_ABDUCTION,
	THUMB_CARPOMETACARPAL_JOINT_FLEXION,
	THUMB_PROXIMAL_PHALANGEAL_JOINT_FLEXION,
	THUMB_DISTAL_PHALANGEAL_JOINT_FLEXION,

	INDEX_METACARPOPHALANGEAL_JOINT_ABDUCTION,
	INDEX_METACARPOPHALANGEAL_JOINT_FLEXION,
	INDEX_PROXIMAL_PHALANGEAL_JOINT_FLEXION,
	INDEX_DISTAL_PHALANGEAL_JOINT_FLEXION,

	MIDDLE_METACARPOPHALANGEAL_JOINT_ABDUCTION,
	MIDDLE_METACARPOPHALANGEAL_JOINT_FLEXION,
	MIDDLE_PROXIMAL_PHALANGEAL_JOINT_FLEXION,
	MIDDLE_DISTAL_PHALANGEAL_JOINT_FLEXION,

	RING_METACARPOPHALANGEAL_JOINT_ABDUCTION,
	RING_METACARPOPHALANGEAL_JOINT_FLEXION,
	RING_PROXIMAL_PHALANGEAL_JOINT_FLEXION,
	RING_DISTAL_PHALANGEAL_JOINT_FLEXION,

	PINKY_METACARPOPHALANGEAL_JOINT_ABDUCTION,
	PINKY_METACARPOPHALANGEAL_JOINT_FLEXION,
	PINKY_PROXIMAL_PHALANGEAL_JOINT_FLEXION,
	PINKY_DISTAL_PHALANGEAL_JOINT_FLEXION
};

class HandPose :
		public Eigen::Vector<float, HAND_DOFS> {

public:
	static const stdx::vector<int> start;
	static const stdx::vector<int> dofs;

	static bool inline isAnAbductionAngle(const HandDofs &dof) {
		switch (dof) {
		case HandDofs::THUMB_CARPOMETACARPAL_JOINT_ABDUCTION    :
		case HandDofs::INDEX_METACARPOPHALANGEAL_JOINT_ABDUCTION :
		case HandDofs::MIDDLE_METACARPOPHALANGEAL_JOINT_ABDUCTION:
		case HandDofs::RING_METACARPOPHALANGEAL_JOINT_ABDUCTION  :
		case HandDofs::PINKY_METACARPOPHALANGEAL_JOINT_ABDUCTION :
			return true;

		default:
			return false;
		}
	}

	static bool inline isAFlexionAngle(const HandDofs &dof) {
		switch (dof) {
		case HandDofs::THUMB_CARPOMETACARPAL_JOINT_FLEXION    :
		case HandDofs::THUMB_PROXIMAL_PHALANGEAL_JOINT_FLEXION :
		case HandDofs::THUMB_DISTAL_PHALANGEAL_JOINT_FLEXION   :
		case HandDofs::INDEX_METACARPOPHALANGEAL_JOINT_FLEXION :
		case HandDofs::INDEX_PROXIMAL_PHALANGEAL_JOINT_FLEXION :
		case HandDofs::INDEX_DISTAL_PHALANGEAL_JOINT_FLEXION   :
		case HandDofs::MIDDLE_METACARPOPHALANGEAL_JOINT_FLEXION:
		case HandDofs::MIDDLE_PROXIMAL_PHALANGEAL_JOINT_FLEXION:
		case HandDofs::MIDDLE_DISTAL_PHALANGEAL_JOINT_FLEXION  :
		case HandDofs::RING_METACARPOPHALANGEAL_JOINT_FLEXION  :
		case HandDofs::RING_PROXIMAL_PHALANGEAL_JOINT_FLEXION  :
		case HandDofs::RING_DISTAL_PHALANGEAL_JOINT_FLEXION    :
		case HandDofs::PINKY_METACARPOPHALANGEAL_JOINT_FLEXION :
		case HandDofs::PINKY_PROXIMAL_PHALANGEAL_JOINT_FLEXION :
		case HandDofs::PINKY_DISTAL_PHALANGEAL_JOINT_FLEXION   :
			return true;

		default:
			return false;
		}
	}

	using Eigen::Vector<float, HAND_DOFS>::Vector;

	HandPose() {
		fill(0);
		hand_rotation() = Eigen::Vector4f(0, 0, 0, 1);
	}

	inline
	void
	clamp(const HandPose &lower, const HandPose &upper) {
		*this = Eigen::max(Eigen::min(*this, upper), lower);
	}

	inline
	const Affine3f
	matrix(HandModel::Part part) const {
		switch(part) {
		case HandModel::INDEX_JOINT_D:
		case HandModel::MIDDLE_JOINT_D:
		case HandModel::RING_JOINT_D:
		case HandModel::PINKY_JOINT_D:
			return Affine3f(
					AngleAxisf((*this)(start[HandModel::Part(part - 2)] * (2.0f / 3)), Vector3f::UnitX())
				);

		default:
			switch(dofs[part]) {
			case 7:
				return Affine3f(
					Translation<float, 3>(this->block<3, 1>(start[part] + 4, 0)) *
					Quaternion <float   >(this->block<4, 1>(start[part]    , 0)).normalized()
				);

			case 2:
				return Affine3f(
					AngleAxisf((*this)(start[part] + 1), Vector3f::UnitX()) *
					AngleAxisf((*this)(start[part]    ), Vector3f::UnitZ())
				);

			case 1:
				return Affine3f(
					AngleAxisf((*this)(start[part]), Vector3f::UnitX())
				);

			case 0:
			default:
				return Affine3f(
					Matrix4f::Identity()
				);
			}
		}
	}

	inline
	Block<Vector<float, HAND_DOFS>, 4, 1>
	hand_rotation() {
		return block<4, 1>(0, 0);
	}

	inline
	const Block<const Vector<float, HAND_DOFS>, 4, 1>
	hand_rotation() const {
		return block<4, 1>(0, 0);
	}

	inline
	Block<Vector<float, HAND_DOFS>, 3, 1>
	hand_translation() {
		return block<3, 1>(4, 0);
	}

	inline
	const Block<const Vector<float, HAND_DOFS>, 3, 1>
	hand_translation() const {
		return block<3, 1>(4, 0);
	}

	inline
	Block<Vector<float, HAND_DOFS>>
	operator[](HandModel::Part part) {
		return this->block(start[part], 0, dofs[part], 1);
	}

	inline
	const Block<const Vector<float, HAND_DOFS>>
	operator[](HandModel::Part part) const {
		return this->block(start[part], 0, dofs[part], 1);
	}
};

template <class T, int R, int C, int O, int M, int N>
std::istream& operator>>(std::istream &stream, Eigen::Matrix<T, R, C, O, M, N> &matrix) {
	T number;
	for (int i = 0; i < matrix.rows(); i++)
		for (int j = 0; j < matrix.cols(); j++)
			stream >> matrix(i, j);
	return stream;
}

} /* namespace msc */
} /* namespace lcg */

#endif /* LCG_MSC_HAND_MODEL_HANDPOSE_H_ */
