#ifndef SKINDETECTOR_H_
#define SKINDETECTOR_H_

#include <cutils/wrappers.h>
#include <msc/Module.h>
#include <msc/types.h>
#include <msc/skindetector/SkinModel.h>
#include <t3/SingletonClass.h>

#include <memory>

namespace lcg {
namespace msc {

class SkinDetector :
		public t3::SingletonClass<SkinDetector>,
		public Module {

	friend SingletonBase;

	std::unique_ptr<SkinModel> skinModel;

protected:
	SkinDetector();

public:
	cutils::device<ObservedSkinImage> observedSkinOutput;

	virtual void run();
};

} /* namespace lcg::msc */
} /* namespace lcg */

#endif /* SKINDETECTOR_H_ */
