#ifndef LCG_MSC_GAUSSIANSKINMODEL_H_
#define LCG_MSC_GAUSSIANSKINMODEL_H_

#include <cuccl/cuccl.h>
#include <cutils/types.h>
#include <msc/GaussianModel.h>
#include <msc/types.h>
#include <msc/skindetector/SkinModel.h>
#include <t3/SingletonClass.h>

namespace lcg {
namespace msc {

class GaussianSkinModel :
		public SkinModel {

	GaussianModel skinDist;

public:
	static GaussianSkinModel* getDefault();

	__host__ __device__
	GaussianSkinModel(const GaussianModel &skin_distribution);

	//TODO: Detection is a responsibility of the module, this class represent only the probability calculation strategy
	virtual void detect();

	__host__ __device__
	float posterior(const cutils::vec2f &point) const;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* LCG_MSC_GAUSSIANSKINMODEL_H_ */
