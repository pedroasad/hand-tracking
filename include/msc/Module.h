#ifndef MSCMODULE_H_
#define MSCMODULE_H_

namespace lcg {
namespace msc {

struct Module {
	virtual ~Module();
	virtual void run() = 0;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* MSCMODULE_H_ */
