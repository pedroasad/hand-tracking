#ifndef CLOCK_H_
#define CLOCK_H_

#include <msc/Module.h>
#include <stdx/string.h>
#include <t3/SingletonClass.h>

#include <chrono>
#include <fstream>

namespace lcg {
namespace msc {

class Clock:
	public t3::SingletonClass<Clock> {

	friend SingletonBase;

public:
	typedef int Tag;
	typedef std::chrono::microseconds TimeUnit;

	TimeUnit check();
	void logTime(const stdx::string &message);

private:
	typedef std::chrono::high_resolution_clock RealClock;
	typedef RealClock::time_point TimePoint;

	bool initialized;
	TimePoint epoch;
	std::ofstream timeFile;

	Clock();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* CLOCK_H_ */
