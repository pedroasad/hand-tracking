#ifndef MSCHANDPOSERENDERER_H_
#define MSCHANDPOSERENDERER_H_

#include <Eigen/Dense>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <msc/HandModel.h>
#include <msc/HandPose.h>
#include <msc/KinectSpecs.h>
#include <msc/PSOOptions.h>
#include <msc/Runtime.h>
#include <msc/System.h>
#include <msc/types.h>
#include <msc/constants.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <tucano/framebuffer.hpp>
#include <tucano/effects/phongshader.hpp>
#include <tucano/texture.hpp>
#include <tucano/utils/flycamera.hpp>
#include <tucano/utils/trackball.hpp>
#include <tucano/utils/path.hpp>
#include <tucano/shapes/camerarep.hpp>

#include <initializer_list>

namespace lcg {
namespace msc {

struct HandPoseRenderer {

	//TODO: Should probably be a SingletonClass but, then, how to receive a certain window?
	HandPoseRenderer();

	template <class InputIterator>
	void renderPosesToFbo(InputIterator begin, InputIterator end) {
		fbo.bind();
		system().renderTarget->forceContext();
		renderStuff(begin, end);

		//TODO: This seems to be a way of directly mapping the framebuffer into an accessible device pointer.
		//	cudaGraphicsResource_t framebuffer_rsc;
		//	size_t framebuffer_size;
		//	float *framebuffer_devptr;
		//	cudaGraphicsGLRegisterBuffer(&framebuffer_rsc, animcamera->fbo.getId(), cudaGraphicsRegisterFlagsReadOnly);
		//	cudaGraphicsMapResources(1, &framebuffer_rsc);
		//	cudaGraphicsResourceGetMappedPointer((void**) &framebuffer_devptr, &framebuffer_size, framebuffer_rsc);
		//	std::cout << framebuffer_size << std::endl;
		//	cudaGraphicsUnmapResources(1, &framebuffer_rsc);
		//	cudaGraphicsUnregisterResource(framebuffer_rsc);

		// Convert rendered depth maps to 16-bit format
		//TODO: Buffer is being downloaded, converted, and then uploaded again
		stdx::vector<float> renderedDepthFloatVector;
		fbo.readDepthBuffer(renderedDepthFloatVector);

		cv::Mat renderedDepthFloatMatrix(
			cv::Size(KinectSpecs::width, KinectSpecs::height * PSOOptions::instance().particles),
			CV_32FC1,
			&renderedDepthFloatVector[0]
		);

		cv::Mat renderedDepthMatrix(
			cv::Size(KinectSpecs::width, KinectSpecs::height * PSOOptions::instance().particles),
			CV_16UC1,
			&renderedDepthBuffers[0]
		);

		//TODO: Document this: the vertex shader maps the z coordinate range [0.4m, 4.0m] into [0, 1], so we need to map it back to 16-bit values here
		float alpha = 1000 * (KinectSpecs::instance().far - KinectSpecs::instance().near);//(65536.0 / KinectSpecs::instance(). far) *  (KinectSpecs::instance(). far - KinectSpecs::instance().near) / 2;
		float beta  = 1000 * KinectSpecs::instance().near;//(65536.0 / KinectSpecs::instance(). far) * ((KinectSpecs::instance(). far - KinectSpecs::instance().near) / 2 + KinectSpecs::instance().near);

		renderedDepthFloatMatrix.convertTo(renderedDepthMatrix, CV_16U, alpha, beta);

		thrust::copy(
			renderedDepthBuffers.begin(), renderedDepthBuffers.end(),
			renderedDepthOutputs.begin()
		);
	}

	void renderPosesToFbo(std::initializer_list<HandPose> poses);

	template <class InputIterator>
	void renderPosesToWindow(InputIterator begin, InputIterator end) {
		int division = ceil(sqrt(end - begin));
		fbo.unbind();
		renderStuff(begin, end, division, division, true);
		system().renderTarget->swapBuffers();
	}

	void renderPosesToWindow(std::initializer_list<HandPose> poses);

	template <class InputIterator>
	void renderStuff(InputIterator begin, InputIterator end, int rows = 1, int cols = 1, bool invertY = false) {
		glClearColor(1.0, 1.0, 1.0, 0.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		int w = KinectSpecs::width  / cols;
		int h = KinectSpecs::height / rows;
		int i = 0;
		//TODO: Take care now, to render only as many poses at once as the framebuffer can hold
		for (InputIterator pose = begin; pose != end; pose++) {
			Eigen::Vector4f viewport(
				w * (i % cols), h * (i / cols),
				w, h
			);

			camera.setViewport(viewport);
			light .setViewport(viewport);

			hand_model.apply(*pose);
			hand_model.render(camera, light, invertY);

			i++;
		}
	}

	void renderStuff(std::initializer_list<HandPose> poses, int rows = 1, int cols = 1, bool invertY = false);

    //TODO: Create a method to attach a specific window, instead of hard-wiring it in the constructor
    Tucano::Framebuffer fbo;
	thrust::device_vector<RenderedDepthImage> renderedDepthOutputs;

private:
	HandModel hand_model;
	Tucano::Effects::Phong phong;
	Tucano::Camera camera;
	Tucano::Camera light;
	thrust::host_vector<RenderedDepthImage> renderedDepthBuffers;
	thrust::host_vector<HandPose> swarm;
};

} /* namespace msc */
} /* namespace lcg */

#endif /* MSCHANDPOSERENDERER_H_ */
