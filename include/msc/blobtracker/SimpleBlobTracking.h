#ifndef LCG_MSC_SIMPLEBLOBTRACKING_H_
#define LCG_MSC_SIMPLEBLOBTRACKING_H_

#include <cuccl/label_equivalence.h>
#include <msc/types.h>
#include <msc/blobtracker/BlobTracking.h>

namespace lcg {
namespace msc {

class SimpleBlobTracking :
		public BlobTracking {

	cuccl::label_equivalence cucclalg;
	cutils::hosted<some_properties<>> relevant_blobs;
	cutils::device<some_properties<>> relevant_blobs_dev;
	cutils::device<cuccl::full_label_image> relabels;
	cutils::hosted<cuccl::full_label_image::element_type> num_blobs;

	void filter_blobs();
	void perform_ccl();

public:
	virtual void track();
};

} /* namespace lcg::msc */
} /* namespace lcg */

#endif /* LCG_MSC_SIMPLEBLOBTRACKING_H_ */
