#ifndef LCG_MSC_BLOBTRACKER_H_
#define LCG_MSC_BLOBTRACKER_H_

#include <cuccl/label_equivalence.h>
#include <cutils/wrappers.h>
#include <msc/Module.h>
#include <msc/blobtracker/BlobTracking.h>
#include <t3/SingletonClass.h>

#include <memory>

namespace lcg {
namespace msc {

class BlobTracker :
		public Module,
		public t3::SingletonClass<BlobTracker> {

	friend SingletonBase;

	std::unique_ptr<BlobTracking> trackingStrategy;

protected:
	BlobTracker();

public:
	//TODO: Add another output for the blob info vector
	cutils::device<cuccl::full_label_image> blobLabelsOutput;

	virtual void run();
};

} /* namespace lcg::msc */
} /* namespace lcg */

#endif /* LCG_MSC_BLOBTRACKER_H_ */
