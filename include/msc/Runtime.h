/*
 * Runtime.h
 *
 *  Created on: 13/02/2016
 *      Author: pedro
 */

#ifndef RUNTIME_H_
#define RUNTIME_H_

#include <t3/SingletonClass.h>

namespace lcg {
namespace msc {

struct Runtime :
		t3::SingletonClass<Runtime> {

	friend SingletonBase;

	int maxParticlesPerRender();

private:
	Runtime();
};

} /* namespace msc */
} /* namespace lcg */

#endif /* RUNTIME_H_ */
