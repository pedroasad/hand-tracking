from SkinModel import SkinModel
from unittest  import TestCase

class SkinModel_test(TestCase):
	def setUp(self):
		self.skinModel = SkinModel()

	def tearDown(self):
		pass

	def addSkinPoints(self):
		"""Updates the skin model with 30 skin samples.

		The points belong to the 3x3 square centered at (128, 128) and have the
		following occurences
		1  4 1
		4 10 4
		1  4 1
		"""
		skinPoints =\
			[(127,127)] * 1 +\
			[(128,127)] * 4 +\
			[(129,127)] * 1 +\
			[(127,128)] * 4 +\
			[(128,128)] * 10+\
			[(129,128)] * 4 +\
			[(127,129)] * 1 +\
			[(128,129)] * 4 +\
			[(129,129)] * 1

		for p in skinPoints:
			self.skinModel.addSkin(p)

	def addNonSkinPoints(self):
		"""Updates the skin model with 60 non-skin samples.

		The points belong to the 3x3 square centered at (130, 128) and have the
		following occurences
		2  8 2
		8 20 8
		2  8 2
		"""
		skinPoints =\
			[(129,127)] * 2 +\
			[(130,127)] * 8 +\
			[(131,127)] * 2 +\
			[(129,128)] * 8 +\
			[(130,128)] * 20+\
			[(131,128)] * 8 +\
			[(129,129)] * 2 +\
			[(130,129)] * 8 +\
			[(131,129)] * 2

		for p in skinPoints:
			self.skinModel.addNonSkin(p)

	def test_InitialProbabilitiesAreNull(self):
		"""
		The likelihood of any 2D color in the reduced (Y)UV space must be
		initially 0, since no images have been fed to the system so far.
		"""
		for u in range(256):
			for v in range(256):
				self.assertAlmostEqual(self.skinModel.posterior((u,v,)), 0)

	def test_SkinLikelihoodsAfterAddingOnlySkinPoints(self):
		"""
		After adding some skin points into the sampling space, the resulting
		probabilities must change.

		Since the skin model is not updated with non-skin samples, the
		probability of finding a skin pixel -- P(s) -- is 100%. Likewise, the
		probability of finding color c given it is a sking color -- P(c|s) --
		equals the likelihood of finding color c, so P(s|c) = 100% in all cases.
		"""
		self.addSkinPoints()

		self.assertAlmostEqual(self.skinModel.prior(), 1)

		self.assertAlmostEqual(self.skinModel.posterior((127,127)), 1)
		self.assertAlmostEqual(self.skinModel.posterior((128,127)), 1)
		self.assertAlmostEqual(self.skinModel.posterior((129,127)), 1)
		self.assertAlmostEqual(self.skinModel.posterior((127,128)), 1)
		self.assertAlmostEqual(self.skinModel.posterior((128,128)), 1)
		self.assertAlmostEqual(self.skinModel.posterior((129,128)), 1)
		self.assertAlmostEqual(self.skinModel.posterior((127,129)), 1)
		self.assertAlmostEqual(self.skinModel.posterior((128,129)), 1)
		self.assertAlmostEqual(self.skinModel.posterior((129,129)), 1)

	def test_SkinLikelihoodsAreNullAfterAddingOnlyNonSkinPoints(self):
		"""
		After adding some skin points into the sampling space, the resulting
		probabilities must change.
		"""
		self.addNonSkinPoints()

		self.assertAlmostEqual(self.skinModel.prior(), 0)

		for u in range(256):
			for v in range(256):
				self.assertAlmostEqual(self.skinModel.posterior((u,v)), 0)

	def test_SkinLikelihoodsAfterAddingSkinAndNonskinPoints(self):
		"""
		After adding some skin points into the sampling space, the resulting
		probabilities must change.

		All tested values can be easily understood by directly applying Baye's
		theorem
			P(s|c) = P(c|s) P(s) / P(c)
		and observing the number of occurrences of skin and non-skin samples.
		These sets have the [(129, 127), (129, 128), (129, 129)] column in
		common, which causes values different from 1, which comprise the most
		interesting part of the test.
		"""
		self.addSkinPoints()
		self.addNonSkinPoints()

		self.assertAlmostEqual(self.skinModel.prior(), 1.0 / 3)

		self.assertAlmostEqual(self.skinModel.posterior((127,127)),   1.0)
		self.assertAlmostEqual(self.skinModel.posterior((128,127)),   1.0)
		self.assertAlmostEqual(self.skinModel.posterior((129,127)), \
				(1.0 / 30) * (1.0 / 3) / ( 3.0 / 90))

		self.assertAlmostEqual(self.skinModel.posterior((127,128)),   1.0)
		self.assertAlmostEqual(self.skinModel.posterior((128,128)),   1.0)
		self.assertAlmostEqual(self.skinModel.posterior((129,128)), \
				(4.0 / 30) * (1.0 / 3) / (12.0 / 90))

		self.assertAlmostEqual(self.skinModel.posterior((127,129)),  1.0)
		self.assertAlmostEqual(self.skinModel.posterior((128,129)),  1.0)
		self.assertAlmostEqual(self.skinModel.posterior((129,129)), \
				(1.0 / 30) * (1.0 / 3) / ( 3.0 / 90))

if __name__ == '__main__':
	main()
