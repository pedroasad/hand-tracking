#!/usr/bin/python
import cv2
import sys
import numpy as np
from SkinDetector import SkinDetector
from term import *

detector = None
matrix   = None

welcomeMsg = """Bayesian Color-based Skin Detector Benchmark Tool (November, 2014), by
	Pedro Asad -- pasad@cos.ufrj.br -- www.lcg.ufrj.br/Members/pasad"""

def atStart(dbfile, skinfile, colorfile):
	global detector
	global matrix

	detector = SkinDetector(skinfile, colorfile)
	matrix   = np.zeros((2,2))

def onFrame(frame, file_orig, file_skin):
	bgr_orig = cv2.imread(file_orig)
	bgr_skin = cv2.imread(file_skin, 0)

	bgr_orig = cv2.GaussianBlur(bgr_orig, (3,3), 0)
	yuv_orig = cv2.cvtColor(bgr_orig, cv2.COLOR_BGR2YUV)

	mask_skin = detector.skinMask(yuv_orig)
	
	count = np.size(mask_skin)
	white = np.sum(bgr_skin == 255)
	black = count - white

	matrix[0,0] += float(np.sum((bgr_skin == 255) &   mask_skin)) / white
	matrix[0,1] += float(np.sum((bgr_skin == 255) & ~ mask_skin)) / white
	matrix[1,0] += float(np.sum((bgr_skin ==   0) &   mask_skin)) / black
	matrix[1,1] += float(np.sum((bgr_skin ==   0) & ~ mask_skin)) / black

def atEnd(frames):
	writeLine('Confusion matrix:\n')
	print matrix / frames

CommandLineDatasetApplication(welcomeMsg, atStart, onFrame, atEnd).run()
