#!/usr/bin/python
import unittest
import numpy
import math

class TestMatrixByArrayOfVectorsMultiplication(unittest.TestCase):
	def test(self):
		shape = (4, 4, 2, 1)
		data  = numpy.zeros(shape)

		mean = numpy.matrix([[12], [6]])
		cov  = numpy.matrix([[2.5, -1], [-1, 1.0]])

		xdata = numpy.random.randn(shape[0], shape[1]) * cov[0,0] + mean[0,0]
		ydata = numpy.random.randn(shape[0], shape[1]) * cov[1,1] + mean[1,0]

		data[:,:,0,0] = xdata
		data[:,:,1,0] = ydata

		datacov = numpy.matrix(numpy.cov([xdata.flatten(), ydata.flatten()]))

		expected = numpy.zeros(shape)
		for y in range(shape[0]):
			for x in range(shape[1]):
				vec = numpy.matrix(data[y,x])

				zscore = - (vec - mean).transpose() * numpy.linalg.inv(cov) * (vec - mean) / 2
				denom  = math.sqrt((2 * math.pi)**2 * numpy.linalg.det(cov))

				expected[y,x] = math.exp(zscore) / denom

		result = numpy.zeros(shape)
		for sample in numpy.ndindex(shape[:2]):
			vec = numpy.matrix(data[sample])

			zscore = - (vec - mean).transpose() * numpy.linalg.inv(cov) * (vec - mean) / 2
			denom  = math.sqrt((2 * math.pi)**2 * numpy.linalg.det(cov))

			result[sample] = math.exp(zscore) / denom

		self.assertTrue(numpy.all(result == expected))

if __name__ == "__main__":
	unittest.main()
