#version 430

in vec4 color;
in vec3 normal;
in vec4 vert;

out vec4 out_Color;

uniform float depthCorrection;
uniform mat4 lightViewMatrix;

void main(void) {
	vec3 lightDirection = normalize((lightViewMatrix * vec4(0.0, 0.0, 1.0, 0.0)).xyz);

	vec3 lightReflection = reflect(-lightDirection, normal);
	vec3 eyeDirection = -normalize(vert.xyz);

	float shininess = 100.0;

	vec4 ambientLight = color * 0.4;
	vec4 diffuseLight = color * 0.6 * max(dot(lightDirection, normal),0.0);
	vec4 specularLight = vec4(1.0) *  max(pow(dot(lightReflection, eyeDirection), shininess),0.0);

	out_Color = vec4(ambientLight.xyz + diffuseLight.xyz + specularLight.xyz,1.0);
	
	gl_FragDepth = gl_FragCoord.z + depthCorrection;
}
